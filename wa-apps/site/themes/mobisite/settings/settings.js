jQuery(function ($) {
    var theme_tabs = {
        last_setting_cookie_name: "theme_" + $.mobisite.theme_id + "_last_setting",
        last_tab_cookie_name: "theme_" + $.mobisite.theme_id + "_last_tab",
        last_scroll_cookie_name: "theme_" + $.mobisite.theme_id + "_last_scroll",
        cookie: function () {
            var add = function (name, value) {
                document.cookie = name + "=" + value + ";path=/";
            };
            var remove = function (name) {
                document.cookie = name + "=null;max-age=0;path=/";
            };
            var get = function (name) {
                var allCookies = document.cookie;
                var pos = allCookies.indexOf(name + "=");
                if (pos != -1) {
                    var start = pos + name.length + 1;
                    var end = allCookies.indexOf(";", start);
                    if (end == -1) {
                        end = allCookies.length;
                    }
                    var value = allCookies.substring(start, end);
                    return decodeURIComponent(value);
                }
            };
            var setScrollTop = function () {
                add(theme_tabs.last_scroll_cookie_name, $(document).scrollTop());
            };
          return {
              add: add,
              remove: remove,
              get: get,
              setScrollTop: setScrollTop
          }
        }(),
        getOptionId: function (option_name) {
            if (typeof option_name == "string") {
                return option_name.replace(/settings\[|parent_settings\[|parent_image\[|image\[|\]/g, '');
            } else {
                return option_name;
            }
        },
        init: function (tabs_settings, options) {
            var self = this;
            this.$theme_settings = $("#theme-settings");
            this.settings = [];
            this.settings_by_group = [];
            this.group_id = 0;
            this.tabs_by_group_id = [];

            self.$theme_settings.find(".fields").find("> .heading, > .field").each(function () {
                var $this = $(this);
                if ($this.hasClass("heading") && typeof self.settings_by_group[self.group_id] != "undefined") {
                    self.group_id++;
                }
                if (!self.settings_by_group[self.group_id]) {
                    self.settings_by_group[self.group_id] = [];
                }
                var option_id = $this.find("[name*='settings[']").attr("name") || 'header';
                var option_title = $this.find(".name").text();
                $this.addClass("field__" + theme_tabs.getOptionId(option_id));
                self.settings_by_group[self.group_id].push({
                    id: option_id,
                    tpl: $this
                });
                self.settings[option_id] = {
                    tpl: $this,
                    section: $this.prevAll(".heading").eq(0).text()
                }
            });
            for (tab_id in tabs_settings) {
                if (tabs_settings.hasOwnProperty(tab_id)) {
                    self.settings_by_group.forEach(function (group, index) {
                        group.forEach(function (o) {
                            if ($.inArray(theme_tabs.getOptionId(o.id), tabs_settings[tab_id]['options']) != -1) {
                                if (typeof self.tabs_by_group_id[tab_id] == "undefined") {
                                    self.tabs_by_group_id[tab_id] = {
                                        'id': index
                                    };
                                }
                            }
                        });
                    });
                }
            }

            /* Формирование вкладок */
            var $custom_theme_settings = $('<div class="custom-theme-settings"></div>');
            var $custom_theme_tabs = $('<ul class="tabs custom-theme-tabs"></ul>');
            var $custom_theme_contents = $('<div class="tab-content custom-theme-contents"></div>');
            for (tab_id in self.tabs_by_group_id) {
                if (self.tabs_by_group_id.hasOwnProperty(tab_id)) {
                    var $custom_theme_content = $('<div class="custom-theme-contents__item custom-theme-contents__' + tab_id +'" data-tab_id="' + tab_id + '"></div>');
                    self.settings_by_group[self.tabs_by_group_id[tab_id]['id']].forEach(function (setting, index) {
                        $custom_theme_content.append($(setting.tpl));
                    });
                    $custom_theme_tabs.append($('<li class="custom-theme-tabs__item custom-theme-tabs__item-' + tab_id + '" data-tab_id="' + tab_id + '"><a href="javascript:void(0);">' + tabs_settings[tab_id]['locale'][$.mobisite.locale] + '</a></li>'));
                    $custom_theme_contents.append($custom_theme_content);
                }
            }
            var $other_settings = self.$theme_settings.find(".fields.form > *").not(".wa-design-save-panel");
            var $custom_theme_contents_others = $('<div class="custom-theme-contents__item custom-theme-contents___others"></div>').append($other_settings);
            var $other_field = $other_settings.filter(function (value) {
                return !$(this).hasClass("heading");
            });
            if ($other_field.length) {
                $custom_theme_tabs.append($('<li class="custom-theme-tabs__item custom-theme-tabs__item-_others" data-tab_id="_others"><a href="javascript:void(0);">' + tabs_settings['_others']['locale'][$.mobisite.locale] + '</a></li>'));
            }
            $custom_theme_settings.append($custom_theme_tabs);
            $custom_theme_contents.append($custom_theme_contents_others);
            $custom_theme_settings.append($custom_theme_contents);
            self.$theme_settings.prepend($custom_theme_settings);
            if (typeof options.events != 'undefined') {
                options.events();
            }
            if (typeof options.onBuilt != 'undefined') {
                options.onBuilt();
            }
        }
    };
    theme_tabs.init(
        {
            /* Настройки вкладок */
            'decoration': {
                locale: {
                    'ru_RU': 'Основные настройки',
                    'en_US': 'Basic settings'
                },
                options: ['logo', 'color_scheme']
            },
            'home': {
                locale: {
                    'ru_RU': 'Главная страница',
                    'en_US': 'Home page'
                },
                options: ['home_product_set_id', 'home_slider_source']
            },
            'catalog': {
                locale: {
                    'ru_RU': 'Каталог и карточка',
                    'en_US': 'Catalog and product card'
                },
                options: ['default_list_view', 'shop_lazyloading']
            },
            '_others': {
                locale: {
                    'ru_RU': 'Прочие настройки',
                    'en_US': 'Other settings'
                },
                options: []
            }
        },
        {
        onBuilt: function () {
            var last_setting_cookie_value = theme_tabs.cookie.get(theme_tabs.last_setting_cookie_name);
            var last_tab_cookie_value = theme_tabs.cookie.get(theme_tabs.last_tab_cookie_name);
            var last_scroll_cookie_value = theme_tabs.cookie.get(theme_tabs.last_scroll_cookie_name);
            if (last_setting_cookie_value) {
                var $last_setting = $(".field__" + last_setting_cookie_value);
                $("#theme-settings").find($(".custom-theme-tabs__item-" + $last_setting.closest(".custom-theme-contents__item").data("tab_id"))).trigger('push');
            } else if (last_tab_cookie_value) {
                $("#theme-settings").find($(".custom-theme-tabs__item-" + last_tab_cookie_value)).trigger('push');
            } else {
                $("#theme-settings").find(".custom-theme-tabs__item").eq(0).trigger('push');
            }
            if (last_scroll_cookie_value) {
                setTimeout(function () {
                    $("html, body").animate({scrollTop: last_scroll_cookie_value}, 0);
                }, 0);
            }
        },
        events: function () {
            $(document).on("click push", ".custom-theme-tabs__item", function (e) {
                $this = $(this);
                var tab_id = $this.data("tab_id");
                $(".custom-theme-tabs__item.selected").removeClass("selected");
                $(".custom-theme-contents__item_active").removeClass("custom-theme-contents__item_active");
                $this.addClass("selected");
                $(".custom-theme-contents__" + tab_id).addClass("custom-theme-contents__item_active");
                if (e.type == "click") {
                    theme_tabs.cookie.add(theme_tabs.last_tab_cookie_name, tab_id);
                    theme_tabs.cookie.setScrollTop();
                    theme_tabs.cookie.remove(theme_tabs.last_setting_cookie_name);
                }
            });
            $("#theme-settings").find("input, select, textarea").change(function () {
                var setting_name = theme_tabs.getOptionId($(this).attr('name'));
                var $field = $(this).closest(".field");
                theme_tabs.cookie.add(theme_tabs.last_setting_cookie_name, setting_name);
                theme_tabs.cookie.setScrollTop();
                theme_tabs.cookie.remove(theme_tabs.last_tab_cookie_name);
            });
            $("#theme-settings").submit(function () {
                theme_tabs.cookie.setScrollTop();
            });
        }
    });
    window._theme_tabs = theme_tabs;
});