if(typeof(String.prototype.trim) === "undefined") {
    String.prototype.trim = function()
    {
        return String(this).replace(/^\s+|\s+$/g, '');
    };
}
var MatchMedia = function( media_query ) {
    var matchMedia = window.matchMedia,
        is_supported = (typeof matchMedia === "function");
    if (is_supported && media_query) {
        return matchMedia(media_query).matches
    } else {
        return false;
    }
};
var mobisite_site = {
    text_input_selectors: "[type=text], [type=email], [type=password], [type=tel], [type=phone], [type=number], textarea",
    exclude_input_parent_classes: ".wa-field-birthday, .wa-field-timezone",
    isTouchDevice: function () {
        return !!('ontouchstart' in window);
    },
    displayAttributeName: function ($elem) {
        $elem.closest(".wa-field").find(".wa-name").show();
    },
    initDots: function (e) {
        if (typeof $.fn.dotdotdot != "undefined") {
            e.dotdotdot({
                    ellipsis: '...',
                    watch: 'window'
            });
        }
    },
    styler: function ($elems) {
        if ($.fn.styler != "undefined") {
            $elems.each(function (i) {
                var $this = $(this);
                if (this.tagName != "SELECT") {
                    $this.styler();
                }
                else {
                    if ($this.find("option").length) {
                        $this.styler();
                    }
                    else {
                        var count = 0;
                        var timer = setInterval(function (j) {
                            if ($this.find("option").length) {
                                $this.styler();
                                clearInterval(timer);
                            }
                            if (count == 50) clearInterval(timer);
                            count++;
                        }, 100);
                    }
                }
            });
        }
    },
    initPlaceholders: function ($selectors) {
        if (typeof $selectors == "undefined") {
            $selectors = $(".wa-field .wa-value");
        }
        $selectors.each(function () {
            var $this = $(this);
            if ($this.find(mobisite_site.text_input_selectors).length) {
                var $wa_field = $this.closest(".wa-field");
                var is_requared = $wa_field.hasClass("wa-required") || $wa_field.find('[required], [required=required]').length;
                if ($wa_field.is(mobisite_site.exclude_input_parent_classes)) {
                    mobisite_site.displayAttributeName($this);
                }
                else {
                    var name;
                    if ($this.find(".field").length) {
                        $this.find(".field").each(function () {
                            name = $(this).children("span").eq(0).text();
                            if (is_requared) {
                                name = name + " *";
                            }
                            $(this).find("input, textarea").not("[type=hidden]").attr("placeholder", name);
                        });
                    }
                    else {
                        name = $this.closest(".wa-field").find(".wa-name").text();
                        if (typeof name === 'string') {
                            name = name.trim();
                        }
                        if (is_requared) {
                            name = name + " *";
                        }
                        $this.find("input, textarea").not("[type=hidden]").attr("placeholder", name);
                    }
                }
            }
            if ($this.find("select").length || $this.find("[type=radio]").length || $this.find("[type=checkbox]").length || $this.closest("#wa-my-info-read-wrapper").length) {
                mobisite_site.displayAttributeName($this);
            }
        });
    },
    measureScrollbar: function () {
        var $body = $("body");
        var $scrollbar_measure = $('<div class="scrollbar-measure" style="width:100vw;"></div>');
        $body.append($scrollbar_measure);
        var outerWidth = $scrollbar_measure.outerWidth();
        $scrollbar_measure.attr("style", "");
        var scrollbarWidth = outerWidth - $scrollbar_measure.innerWidth();
        $body.find(".scrollbar-measure").remove();
        return scrollbarWidth;
    },
    windows: (function () {
        var getPaddingRight = function ($selector) {
            return (typeof $selector.data("paddingright") != "undefined") ? parseFloat($selector.data("paddingright")) : parseFloat($selector.css("paddingRight"));
        };
        var removeOverlay = function () {
            $("body").removeClass("overlay");
            var $header = $(".header-top__inner");
            $header.css({paddingRight: getPaddingRight($header)});
            var overlay_scroll = $.cookie('overlay_scroll');
            if (overlay_scroll) {
                $('html, body').scrollTop(overlay_scroll);
                $.cookie('overlay_scroll', null, { expires: 0, path: '/' });
            }
        };
        var addOverlay = function () {
            var $header = $(".header-top__inner");
            var header_padding_right = getPaddingRight($header);
            $header.data("paddingright", header_padding_right).css({paddingRight: header_padding_right + mobisite_site.measureScrollbar()});
            $.cookie('overlay_scroll', $(window).scrollTop(), { path: '/' });
            $("body").addClass("overlay");
        };
        var fixPopupBottom = function ($popup) {
            if ($popup.find(".popup-bottom").length) {
                $popup.css({marginBottom: $popup.find(".popup-bottom").outerHeight() - 2});
            }
        };
        var closeAllWindows = function () {
            $(".js-popup, .js-dropdown-window").removeClass("open");
            $(".search-block__dropdown_history").remove();
            $(".dropdown-window-toggle_opened").removeClass("dropdown-window-toggle_opened");
            $(".js-topmenu-dropdown").remove();
            removeOverlay();
        };
        var openPopup = function ($popup) {
            closeAllWindows();
            addOverlay();
            $popup.addClass("open");
            fixPopupBottom($popup);
        };
        var removePopup = function ($popup) {
            removeOverlay();
            $popup.remove();
        };
        var openDropdown = function ($dropdown) {
            closeAllWindows();
            $dropdown.addClass("open");
            addOverlay();
        };
        var closeDropdown = function ($dropdown) {
            closeAllWindows();
            $dropdown.removeClass("open");
        };
        return {
            closeAll: closeAllWindows,
            openPopup: openPopup,
            removePopup: removePopup,
            openDropdown: openDropdown,
            closeDropdown: closeDropdown,
            addOverlay: addOverlay
        }
    })(),
    reachGoal: function (goal_id) {
        var _ya_counter = window['yaCounter' + mobisite_site.yaCounterId];
        if (_ya_counter && goal_id) {
            _ya_counter.reachGoal(goal_id);
        }
    },
    search_history: (function () {
        var fn = {
            writeCookie: function (id, text) {
                var cookie = '';
                var result_cookie = [];
                var history_count = 0;
                var history_limit = 10;
                if (window.$.cookie) {
                    cookie = $.cookie('search_history');
                    if (cookie) {
                        if (typeof result_cookie[id] == "undefined") {
                            result_cookie[id] = text;
                        }
                        cookie = cookie.split(';');
                        cookie.forEach(function (item, i) {
                            if (item.length) {
                                var sub = item.split(':');
                                if (sub[0] != id && (history_count + 1) < history_limit) {
                                    result_cookie[sub[0]] = sub[1];
                                    history_count++;
                                } else if (history_count >= history_limit) {
                                    return false;
                                }
                            }
                        });
                    } else {
                        result_cookie[id] = text;
                    }
                    $.cookie('search_history', mobisite_site.search_history.arrayToString(result_cookie.reverse()), { expires: 7, path: '/' });
                }
            },
            arrayToString: function (array) {
                var result = '';
                for (var key in array) {
                    result = result + key + ":" + array[key] + ";"
                }
                return result;
            },
            removeHistoryList: function () {
                if ($(".search-block__dropdown_history").length) {
                    setTimeout(function () {
                        $(".search-block__dropdown_history").remove();
                    }, 50);
                }
            }
        };
        var init = function () {
            var self = this;
            this.stringToArray = function (string) {
                var result = [];
                if (typeof string == "string") {
                    var sections = string.split(';');
                    for (var key in sections) {
                        if (sections[key] != '') {
                            var section = sections[key].split(":");
                            result[section[0]] = section[1];
                        }
                    }
                }
                return result;
            };
            this.getHistoryListHTML = function (array) {
                var $form = $(".search-block_history .search-form");
                var $result = $($form.data("history_tpl"));
                if (Object.keys(array).length) {
                    for (var key in array) {
                        var $result_item = $($form.data("history_tpl_item"));
                        $result_item.find(".search-block__dropdown-link").attr("href", key);
                        $result_item.find(".search-block__dropdown-link-text").text(array[key]);
                        $result.append($result_item);
                    }
                }
                return $result;
            };
            this.getHistoryList = function () {
                var cookie = $.cookie('search_history');
                var cookie_array = self.stringToArray(cookie);
                mobisite_site.search_history.removeHistoryList();
                var $history_form = self.getHistoryListHTML(cookie_array);
                if ($history_form.html()) {
                    $(".search-block_history").append(self.getHistoryListHTML(cookie_array));
                }
            };
            $(".search-block_history .search-form__query").focus(function () {
                if ($(this).val() == '') {
                    self.getHistoryList();
                    var $dropdown_history = $(".search-block__dropdown_history");
                    if ($dropdown_history.length) {
                        $dropdown_history.css({
                            maxHeight: $(window).height() - $dropdown_history.offset().top
                        });
                        mobisite_site.windows.addOverlay();
                    }
                }
            });
            $(".search-block_history .search-form__query").focusout(function () {
                mobisite_site.search_history.removeHistoryList();
            });
            $(document).on(device_action, ".search-block__dropdown_history .search-block__dropdown-link", function () {
                $(".search-block_history .search-form__query").val($(this).find(".search-block__dropdown-link-text").text());
            });
        };
        return {
            init: init,
            writeCookie: fn.writeCookie,
            arrayToString: fn.arrayToString,
            removeHistoryList: fn.removeHistoryList
        }
    })(),
    search: (function () {
        var init = function ($search_field) {
            $search_field.autocomplete({
                source: function(request, response){
                    $.ajax({
                        type: 'GET',
                        dataType: 'json',
                        url : '/search/?ajax=1&query=' + $search_field.val(),
                        success: function(data){
                            response($.map(data, function(item){
                                return {
                                    label: item.label,
                                    value: item.value
                                }
                            }));
                        }
                    });
                },
                open: function( event, ui ) {
                    var $dropdown = $(".search-block__dropdown").not(".search-block__dropdown_history");
                    var offset = $(".search-block__inner").css("paddingBottom").replace("px", "");
                    $dropdown.css({
                        marginTop: "+="+offset,
                        maxHeight: $(window).height() - $dropdown.offset().top - offset
                    });
                    mobisite_site.search_history.removeHistoryList();
                    mobisite_site.windows.addOverlay();
                },
                close: function ( event, ui ) {
                    if (!$(".search-block_ajax").hasClass("js-dropdown-window")) {
                        mobisite_site.windows.closeAll();
                    }
                }
            }).autocomplete("widget").addClass("search-block__dropdown");

            $(document).on(device_action, ".search-block__dropdown-link", function (e) {
                e.preventDefault();
                var $target = $(e.target);
                var $search_field = $(".search-block_ajax .search-form__query");
                var target_text = $target.closest(".search-block__dropdown-link").find(".search-block__dropdown-link-text").text();
                if ($target.hasClass("search-block__dropdown-link-text")) {
                    $search_field.val(target_text);
                    window.location.href = $target.closest(".search-block__dropdown-link").attr("href");
                } else {
                    if ($target.hasClass("search-block__find")) {
                        $search_field.val(target_text);
                        $(".search-block_ajax .search-form").submit();
                    } else if ($target.hasClass("search-block__select")) {
                        $search_field.val(target_text);
                        $search_field.autocomplete("search", target_text);
                    } else {
                        $search_field.val(target_text);
                        window.location.href = $target.closest(".search-block__dropdown-link").attr("href");
                    }
                }
            });

            if ($(".search-block_ajax").length) {
                $(document).on(device_action, ".search-block__dropdown-link-text", function () {
                    var $this = $(this);
                    var $link = $this.closest(".search-block__dropdown-link");
                    var href = $link.attr('href');
                    mobisite_site.search_history.writeCookie(href, $this.text());
                    $(".search-block_ajax .search-form__query").val($this.text());
                    location.href = href;
                });
            }
        };
        return {
            init: init
        }
    })(),
    exclude_expansion: function ($images, expansion) {
        if ($images.length) {
            try {
                if (!$images.length) return $images;

                expansion = expansion || 'svg';
                var pattern = new RegExp('\.' + expansion + '($|\\?)', 'gi');
                $images = $images.filter(function () {
                    return $(this).attr('src').search(pattern) == -1;
                });
                return $images;
            } catch (e) {
                return $images;
                console.error(e);
            }
        } else {
            return $images;
        }

    },
    parseJSON: function (data) {
        return window.JSON && window.JSON.parse ? window.JSON.parse( data ) : (new Function("return " + data))();
    }
};

//var device_action = mobisite_site.isTouchDevice() ? 'touch' : 'click';
var device_action = 'click';

$(document).ready(function() {
    mobisite_site.styler($("input, select").not(
        ".plugin_arrived-box input," +
        ".plugin_arrived-box select," +
        "#wa-my-info-edit-wrapper [name=photo_file]," +
        ".quickorder-body input," +
        ".searchpro__page select," +
        ".searchpro__page input," +
        ".checkout-options [id*='np2_']," +
        ".checkout-options [class*='np2_']," +
        "#js-order-cart input," +
        "#js-order-cart select," +
        "#js-order-form input," +
        "#js-order-form select"
    ));

    mobisite_site.globals = (function () {
        var globals = $('.js-theme-globals').val();
        if (globals) {
            return mobisite_site.parseJSON(globals);
        }
        return {};
    })();

    if (typeof $.fn.slick != "undefined") {
        $(".home-slider__wrapper").slick({
            dots: true,
            arrows: false,
            slidesToShow: 1,
            infinite: true,
            slidesToScroll: 1,
            speed: 300,
            draggable: true,
            swipe: true,
            pauseOnHover: false,
            focusOnSelect: false,
            touchThreshold: 15
        });
        $(".brands-block .list-slider").slick({
            lazyLoad: 'ondemand',
            dots: true,
            arrows: false,
            slidesToShow: 3,
            infinite: true,
            slidesToScroll: 3,
            speed: 300,
            draggable: true,
            swipe: true,
            adaptiveHeight: true,
            pauseOnHover: false,
            focusOnSelect: false,
            touchThreshold: 15,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    mobisite_site.initPlaceholders($(".wa-field .wa-value").not("#create-user-div .wa-value"));

    if (!$(".search-block_smartsearch").length) {
        if ($(".search-block_ajax").length) {
            mobisite_site.search.init($(".search-block_ajax .search-form__query"));
            if ($(".search-block_history").length) {
                mobisite_site.search_history.init();
            }
        }
    }


    $(".dropdown-window-toggle").on(device_action, function (e) {
        var $this = $(this);
        var $target_selector = $($this.data("target"));
        if ($target_selector.length) {
            if (!$target_selector.hasClass("open")) {
                mobisite_site.windows.openDropdown($target_selector);
                $this.addClass("dropdown-window-toggle_opened");
            }
            else {
                mobisite_site.windows.closeDropdown($target_selector);
                $this.removeClass("dropdown-window-toggle_opened");
            }
        }
    });
    $(".search-form__query").on('keyup', function () {
        var $this = $(this);
        var f = $this.closest("form");
        $this.val() != '' ? f.addClass("active") : f.removeClass("active");
    });
    var currency_select = $(".footer-currency select");
    currency_select.change(function () {
        var url = location.href;
        if (url.indexOf('?') == -1) {
            url += '?';
        } else {
            url += '&';
        }
        location.href = url + 'currency=' + $(this).val();
    });

    $(document).on("change", ".list-sorting select", function () {
        location.href = $(this).find("option:selected").data("url");
    });

    $(document).on(device_action, function (e) {
        var $target = $(e.target);
        if ($target.hasClass("overlay")) {
            mobisite_site.windows.closeAll();
        }
    });
    if ($(".shop-regions").length) {
        $(document).on(device_action, ".shop-regions-window.shop-regions-window_show", function (e) {
            if ($(e.target).hasClass("shop-regions-window")) {
                $(this).find(".shop-regions__trigger-hide-window").trigger("click");
            }
        });
    }
    $(".js-button-expand").on(device_action, function (e) {
        $this = $(this);
        $this.toggleClass("open")
            .closest(".js-expand-wrapper").find(".js-hidden").toggleClass("open");
    });

    var dynamicCatalog = function () {
        var self = this;

        this.catalogCategories = {};
        this.loadSubdropdownCatalog = function (category_id) {
            var reuqest_params = {
                ajax: 1,
                action: 'subdropdown_catalog',
                category_id: category_id
            };
            self.addSubdropdownLoading();
            $.ajax({
                url: mobisite_site.globals.search_url + '?' + $.param(reuqest_params),
                dataType: 'json'
            }).done(function (response) {
                self.catalogCategories[category_id] = response.html;
                self.replaceSubdropdownCatalog(category_id);
            });
        };
        this.replaceSubdropdownCatalog = function (category_id) {
            $('.js-topmenu').append((self.catalogCategories[category_id]));
            self.removeSubdropdownLoading();
        };
        this.fetchBlockBySelector = function ($data) {
            return $data.select('.js-topmenu-dropdown').html();
        };
        this.displayCatalogHandle = function (category_id) {
            if (typeof category_id != 'undefined') {
                if (typeof self.catalogCategories[category_id] != 'undefined') {
                    self.replaceSubdropdownCatalog(category_id);
                } else {
                    self.loadSubdropdownCatalog(category_id);
                }
            } else {
                self.close();
            }
        };
        this.closeSubdropdown = function () {
            $('.js-topmenu-dropdown').remove();
        };
        this.addSubdropdownLoading = function () {
            $('.js-topmenu').addClass('topmenu_loading');
        };
        this.removeSubdropdownLoading = function () {
            $('.js-topmenu').removeClass('topmenu_loading');
        };
        this.bindEvents = function () {
            $(document).on(device_action, '.js-dynamic-catalog__link', function (e) {
                var $catalog_link = $(this),
                    subcategories_count = $catalog_link.data('subcategories-count');

                if (subcategories_count) {
                    var category_id = $catalog_link.data('category-id');

                    if (category_id) {
                        e.preventDefault();
                        self.displayCatalogHandle(category_id);
                        return false;
                    }
                }
            });

            $(document).on(device_action, '.js-topmenu-dropdown__header-close', function () {
                self.closeSubdropdown();
            });

            $(document).on(device_action, '.js-topmenu-dropdown__header-back', function () {
                var category_id = $(this).data('parent-id');
                self.closeSubdropdown();
                self.displayCatalogHandle(category_id);
            });
        };

        self.bindEvents();
        return this;
    };
    new dynamicCatalog();

    $('select[name*="[country]"]').on('change', function () {
        setTimeout(function () {
            $('.wa-field-address-region, .wa-field-region').find('select').each(function () {
                var $select = $(this),
                    style =  $select.attr('style');

                setTimeout(function () {
                    if ($select.closest('.jq-selectbox').length) {
                        $select.trigger('refresh');
                            if ($select.attr('style')) {
                                $select.closest('.jq-selectbox').attr('style', $select.attr('style'));
                            } else {
                                $select.closest('.jq-selectbox').removeAttr('style');
                            }
                    } else {
                        mobisite_site.styler($select);
                    }
                }, 500);
            });
        }, 0);
    });

    $(document).on('click', '.wa-dialog-background', function () {
        $(this).closest('.wa-order-dialog').find('.js-close-dialog').trigger('click');
    });
});
