var MatchMedia = function( media_query ) {
    var matchMedia = window.matchMedia,
        is_supported = (typeof matchMedia === "function");
    if (is_supported && media_query) {
        return matchMedia(media_query).matches
    } else {
        return false;
    }
};

function align_height(elem) {
    var elem_height = 0;
    $(elem).each(function() {
        var block_height = parseInt($(this).height());
        if (block_height > elem_height) {
            elem_height = block_height;
        };
    });
    $(elem).height(elem_height);
}

$(document).ready(function() {

	$('.telnumber, .wa-field-phone input[type=text]').mask('+7(999)999-9999');

	$('.full_version .close').click(function(){
		$(this).parent().remove();
	});


    $('.pages_list_button a').click(function(event) {
        event.preventDefault();
        var parent = $(this).parent();

        if ( !parent.hasClass('active') ) {
            parent.addClass('active');
            $('.pages_list').show();
        } else {
            parent.removeClass('active');
            $('.pages_list').hide();
        };
    });


	$('.fast_select_cat').toggle(
		function(){
			$('.fast_cat_list').show();
		},
		function(){
			$('.fast_cat_list').hide();
		}
	);

    
    // Товар в избранное
    var wishlist = new ListInCookie('wishlist_items', 90 *24 * 60 * 60 * 1000, $('.wishlist_count'));
    
    $('.to-wishlist').click(function () {
        var id = $(this).data('product');

        if ($(this).hasClass('active')) {
            wishlist.remove(id);
            $(this).removeClass('active');
            $(this).children('span').text('Добавить в избранное');
        } else {
            wishlist.add(id);
            $(this).addClass('active');
            $(this).children('span').text('Удалить из избранного');
        };

        return false;
    });
});