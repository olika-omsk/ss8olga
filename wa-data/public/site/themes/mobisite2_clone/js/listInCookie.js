function ListInCookie(cookieName, timeToLive, item, size) {
    this.COOKIE_NAME = cookieName,
    this.timeToLive = timeToLive,
    this.label = item,
    this.maxSize = size || 999,
    this.getList = function () {
        var preList = getCookie(this.COOKIE_NAME);
        return typeof(preList) != 'undefined' && preList.length > 0 ? preList.split(',') : [];
    },
    this.setCount = function () {
        var list = this.getList(),
            count = 0;

        if (list.length > 0) {
            count = list.length;
        }

        this.label.text(count);
        if (!this.label.is(":visible") && count > 0) {
            this.label.show();
        }
        return;
    },
    this.add = function (newItem) {
        var list = this.getList(),
            options = {
                'path': '/',
                'expired': new Date(new Date().getTime() + timeToLive).toUTCString()
            };
        if (list.length == this.maxSize) {
            list.shift();
        }

        if (list.indexOf(newItem) == -1)
            list.push(newItem);

        setCookie(this.COOKIE_NAME, list.join(','), options);
        this.setCount();
        return;
    },
    this.remove = function (item) {
        var list = this.getList(),
            index = list.indexOf(item.toString()),
            options = {
                'path': '/',
                'expired': new Date(new Date().getTime() + timeToLive).toUTCString()
            };

        if (index >= 0) {
            list.splice(index, 1);
        }

        setCookie(this.COOKIE_NAME, list.join(','), options);
        this.setCount();
        return;
    }
    this.setCount();
}