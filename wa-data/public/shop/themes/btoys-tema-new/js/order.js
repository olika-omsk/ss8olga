<!-- Begin Me-Talk -->
(function(d, w, m) {
    window.supportAPIMethod = m;
    var s = d.createElement('script');
    s.type ='text/javascript'; s.id = 'supportScript'; s.charset = 'utf-8';
    s.async = true;
    var id = 'a4924c9e591fb395828aebe216f437ee';
    s.src = '//me-talk.ru/support/support.js?h='+id;
    var sc = d.getElementsByTagName('script')[0];
    w[m] = w[m] || function() { (w[m].q = w[m].q || []).push(arguments); };
    if (sc) sc.parentNode.insertBefore(s, sc);
    else d.documentElement.firstChild.appendChild(s);
})(document, window, 'MeTalk');
<!-- End Me-Talk -->

( function($) { "use strict";

    var ShopOrderPage = ( function($) {

        ShopOrderPage = function(options) {
            var that = this;

            // DOM
            that.$wrapper = options["$wrapper"];

            // VARS
            that.urls = options["urls"];

            // DYNAMIC VARS

            // INIT
            that.initClass();
        };

        ShopOrderPage.prototype.initClass = function() {
            var that = this;

            that.initUI();

            that.initThemeCart();

            that.$wrapper.on("click", ".js-clear-cart", function() {
                var wa_order_cart = getCartController(that.$wrapper);
                if (wa_order_cart) {
                    wa_order_cart.clear({
                        confirm: true
                    }).then( function() {
                        location.reload();
                    });
                } else {
                    alert("Error");
                }
            });
        };

        ShopOrderPage.prototype.initUI = function() {
            var that = this;

            initUI();

            that.$wrapper
                .on("wa_order_cart_reloaded", function() {
                    initUI();
                })
                .on("wa_order_form_reloaded", function() {
                    initUI();
                })
                .on("wa_order_form_changed", function() {
                    initUI();
                });

            $(document).on("wa_auth_form_loaded", function(event, data) {
                var $wrapper = $("#" + data.form_wrapper_id);
                if ($wrapper.length) {
                    initUI($wrapper);
                }
            });

            $(document).on("wa_order_dialog_open", function(event, $wrapper) {
                initUI($wrapper);
            });

            function initUI($wrapper) {
                $wrapper = ($wrapper ? $wrapper : that.$wrapper);

                initCheckbox($wrapper);
                initStyledSelect($wrapper);
                initRadio($wrapper);
            }

            function initCheckbox($wrapper) {
                $wrapper.find("input[type=\"checkbox\"]").each( function() {
                    var $input = $(this),
                        is_rendered = $input.data("is_rendered");

                    if (!is_rendered) {
                        render($(this));
                        $input.data("is_rendered", true);
                    }
                });

                function render($input) {
                    var $wrapper = $("<span class=\"s-checkbox\" />").insertBefore($input).prepend($input);
                    var $span = "<span><i class=\"s-icon\"><svg><use xlink:href=\"%icon_uri%\"></use></svg></i></span>".replace("%icon_uri%", that.urls["checkbox-icon"]);

                    $wrapper.append($span);

                    return $wrapper;
                }
            }

            function initRadio($wrapper) {
                $wrapper.find("input[type=\"radio\"]").each( function() {
                    var $input = $(this),
                        is_rendered = $input.data("is_rendered");

                    if (!is_rendered) {
                        render($(this));
                        $input.data("is_rendered", true);
                    }
                });

                function render($input) {
                    var $wrapper = $("<span class=\"s-radio\" />").insertBefore($input).prepend($input);
                    var span = "<span />";

                    $wrapper.append(span);

                    return $wrapper;
                }
            }

            function initStyledSelect($wrapper) {
                $wrapper.find("select").each( function() {
                    var $select = $(this),
                        is_rendered = $select.data("is_rendered");

                    if (!is_rendered) {
                        $("<div class=\"s-styled-select\"><span class=\"s-icon\"></span></div>").insertBefore($select).prepend($select);

                        $select.on("change", setActive);

                        setActive();

                        $select.data("is_rendered", true);
                    }

                    function setActive() {
                        var active_index = $select[0].selectedIndex,
                            active_class = "selected";

                        $select.find("option").each( function(index) {
                            var $option = $(this);
                            if (index === active_index) {
                                $option.addClass(active_class);
                            } else {
                                $option.removeClass(active_class);
                            }
                        })
                    }
                });
            }
        };

        ShopOrderPage.prototype.initThemeCart = function() {
            var that = this;

            var $cart = $("#cart");

            // Header :: Cart
            var Cart = (function($) {

                Cart = function(options) {
                    var that = this;

                    // DOM
                    that.$wrapper = options["$wrapper"];
                    that.$price = options["$price"];

                    // VARS

                    // DYNAMIC VARS

                    // INIT
                    that.initClass();
                };

                Cart.prototype.initClass = function() {
                    var that = this;

                };

                /**
                 * @param {Object} data
                 * */
                Cart.prototype.update = function(data) {
                    var that = this;

                    if (data.formatted_price) {
                        that.$price.html(data.formatted_price);
                        toggle(data.price > 0);
                    }

                    function toggle(show) {
                        var empty_class = "empty";

                        if (show) {
                            that.$wrapper.removeClass(empty_class);
                        } else {
                            that.$wrapper.addClass(empty_class);
                        }
                    }
                };

                return Cart;

            })($);

            var cart = new Cart({
                $wrapper: $cart,
                $price: $cart.find(".cart-total")
            });

            that.$wrapper.on("wa_order_cart_changed", function(event, api) {
                var wa_order_cart = getCartController(that.$wrapper);

                cart.update({
                    price: api.cart.total,
                    formatted_price: wa_order_cart.formatPrice(api.cart.total)
                });
            });
        };

        return ShopOrderPage;

        function getCartController($wrapper) {
            var result = null;

            var $cart = $wrapper.find("#js-order-cart");

            if ($cart.length && $cart.data("controller")) {
                result = $cart.data("controller");

            } else {
                throw new Error("Can't find cart controller");
            }

            return result;
        }

    })(jQuery);

    window.ShopOrderPage = ShopOrderPage;

    if (!$('input[name="auth[data][predpochti4]"]:checked').length)
    {
        $('input[name="auth[data][predpochti4]"][value="Любой"]').closest('div').addClass('checked');
        $('input[name="auth[data][predpochti4]"][value="Любой"]').attr('checked','checked');
    }

})(jQuery);