$(document).ready(function () {
    /*$('.bxslider').bxSlider({ auto : true, pause : 5000, autoHover : true });*/
    /*plugin referral*/
    var $reff = $('table.referral-channels-report');
    if($reff.length){
        $reff.addClass('table');
        $reff.find('tr:first').addClass('coll-name ahover');
        $reff.find('tr:even:not(:first)').addClass('zebra');
    }
    
    var cd = $('#category-description');
    if(cd.length && cd.height() > parseInt(cd.data('cutter'))){
        var btn_cut = $('<i class="btn-cutter expand">'+cd.data('etext')+'</i>').click(function(){
            var self = $(this); self.hide();
            if(self.hasClass('expand')){
                cd.removeClass('cutter').animate({ height: cd.data('orig-height') }, 'slow', function(){
                    self.text(cd.data('ctext')).show();
                });
            }else{
                cd.animate({ height: cd.data('cutter') }, 'slow', function(){
                    $(this).addClass('cutter');
                    self.text(cd.data('etext')).show();
                    $('body,html').animate({ scrollTop: cd.position().top }, 800);
                });
            }
            self.toggleClass('expand');
        });
        cd.data('orig-height',cd.height()).addClass('cutter').css('height',cd.data('cutter')).append(btn_cut);
    }

    $("#main").on('submit', 'form.addtocart', function () {
        var f = $(this);
		if (f.data('url')) {
            var d = $('#dialog');
            var c = d.find('.cart');
            c.addClass('product').load(f.data('url'), function () {
                var top = f.offset().top - 275;
                d.show();

                if($('#dialog-image-product').length) {
                    c.parent().prepend(c.find('h4'));
                    c.css('margin-top', d.find('h4').innerHeight());
                    $('#dialog-image-product').css('top', top)
                }
                c.css('top', top);
                if ((c.height() > c.find('form').height())) {
                    c.css('bottom', 'auto');
                } else {
                    c.css('bottom', '15%');
                }
            });
            return false;
        }
        $.post(f.attr('action') + (sumbolrub ? '?html=1' : ''), f.serialize(), function (response) {
            if (response.status == 'ok') {
                var cart_total = $(".cart-total");
                if ( $(window).scrollTop()>=35 ) {
                    cart_total.closest('#cart').addClass( "fixed" );
    	        }
                cart_total.closest('#cart').removeClass('empty');
                
                if ($("table.cart").length) {
                    $(".content").parent().load(location.href, function () {
                        cart_total.html(response.data.total);
                        $(window).unbind('load.jcarousel, resize.jcarousel');
                        $('ul[class^="products-slider-"]').jcarousel({
                  	        scroll: 1,
                	        buttonNextHTML: '<div><i class="icon-caret-right"></i></div>',
                            buttonPrevHTML: '<div><i class="icon-caret-left"></i></div>',
                            reloadCallback: function(carousel){ carousel.scroll(1, false); carousel.list.css('left', 0); }
                        });
                    });
                } else {
                    var origin = f.closest('li');
                    var block = $('<div></div>').append(origin.html());
                    block.css({
                        'z-index': 10,
                        top: origin.offset().top,
                        left: origin.offset().left,
                        width: origin.width()+'px',
                        height: origin.height()+'px',
                        position: 'absolute',
                        overflow: 'hidden'
                    }).insertAfter('#main').animate({//code origin => .content
                        top: cart_total.offset().top,
                        left: cart_total.offset().left,
                        width: 0,
                        height: 0,
                        opacity: 0.5
                    }, 500, function() {
                        $(this).remove();
                        cart_total.html(response.data.total);
                    });
                    
                    var $scd = f.closest('li').find('.soaring-cart-data').data();
                    if($scd){
                        var $item = $('#soaring-cart li[data-id="'+response.data.item_id+'"]');
                        var cnt = parseInt(f.find('input[name="quantity"]').val()) || 1;
                        
                        if($item.length){
                            var $qty = $item.find('input.soaring-cart-qty');
                            cnt += parseInt($qty.val());
                            $qty.val(cnt);
                            $item.find('.price').html(currency_format($scd.price*cnt, !sumbolrub, $.comfortbuy.currency));//*
                        }else{
                            $scd = $.extend({}, $scd, { id: response.data.item_id, cnt: cnt, price: ''+currency_format($scd.price*cnt, !sumbolrub, $.comfortbuy.currency) });//*
                            $('#soaring-cart ul').prepend(newItem($scd));
                            setSoaringHeight();
                        }
                        
                        $('#soaring-cart').scrollTop( $('#soaring-cart li[data-id="'+response.data.item_id+'"]').position().top );
                    }
                }
                if (response.data.error) {
                    alert(response.data.error);
                }
            } else if (response.status == 'fail') {
                alert(response.errors);
            }
        }, "json");
        return false;
    });
    
    /*brands*/
    var brands = $('.menu-v.brands');
    brands.find('a').prepend('<i class="icon-circle"></i>');
    if(brands.find('li').length>5){
        brands.find('li').filter(':gt(4)').hide();
        brands.append('<li class="align-right"><a id="brands-switch" class="type1" href="#">'+$.comfortbuy.locale.showall+'</a></li>');
        $('#brands-switch').click(function(){
            brands.find('li:hidden').show();
            $(this).parent().hide();
            return false;
        });
    }
    
    $('.filters.ajax form input').change(function () {
        var f = $(this).closest('form');
    	var fields = f.serializeArray();
    	var params = [];
    	for (var i = 0; i < fields.length; i++) {
    		if (fields[i].value !== '') {
    			params.push(fields[i].name + '=' + fields[i].value);
    		}
    	}
    	var url = '?' + params.join('&');
        $(window).lazyLoad && $(window).lazyLoad('sleep');
        var loading_filter = $('<div style="position: absolute; top: 0; left: 0; right: 0; bottom: 0; background: rgba(255,255,255,0.4); z-index: 10;"><i class="icon16 loading" style="position: absolute; top: 50%; margin-top: -8px; left: 50%; margin-left: -8px; border: none !important"></i></div>');
        f.css('position', 'relative').append(loading_filter);
        //$.get(url, function(html) {
        $.get(url+'&_=_', function(html) {
            var tmp = $('<div></div>').html(html),
                h = window.history;

            $('#product-list').html(tmp.find('#product-list').html());
            if(h['pushState']) {
                h['pushState']({
                    category:url
                }, document.title, url);
            }
            $(window).lazyLoad && $(window).lazyLoad('reload');
            // код плагина "Наклейки"
            if (typeof $.autobadgeFrontend !== 'undefined') {
                 $.autobadgeFrontend.reinit();
            }  
            event_category();
            loading_filter.remove();
        });
    });
    
    var event_category = function(){
        $('#products-per-page a').click(function(){
            if(!$(this).hasClass('selected')){
                if($(this).data('pppc')){
                    $.cookie('products_per_page', $(this).data('pppc'), { expires: 30, path: '/'});
                }else{
                    $.cookie('products_per_page', '', { expires: 0, path: '/'});
                }
            }else{ return false; }
        });
        $('#sorting').change(function(){
            location.assign($(this).val());
        });
        $('.view-select span').click(function(){
            var self = $(this);
            if($(this).hasClass('selected')) return false;
            $('.view-select span').removeClass('selected'); $(this).addClass('selected');
            if($(this).data('view')){
                $('ul.product-list').addClass('thumbs').removeClass('list');
                $.cookie('shop_view', 'thumbs', { expires: 30, path: '/'});
            }else{
                $('ul.product-list').addClass('list').removeClass('thumbs');
                $.cookie('shop_view', 'list', { expires: 30, path: '/'});
            }
            return false;
        });
    }
    event_category();
    
    /**
     * new
     */
    preorderVisibilityCheck();
	prepareArcticModal();
	//prepareModalPreorderBtn();

	$('#product-list, .product-list, .products-slider').on('click', '.productpreview-button', function(e){
    	e.preventDefault();
    	var t = $(this);
    	$.fancybox({
    			href: t.prop('href'),
    			type: 'ajax'
    		}, {
    		afterClose:function(){
    			$('.zoomContainer').remove();
    		},
			afterLoad:function(){
				setTimeout(function(){
					var k = 0;
					$('select.sku-feature1 option').each(function(index, element) {
						var nal = $(this).css('color');
						if(nal != 'rgb(170, 170, 170)'){
							k = k + 1
							if(k == 1){
								$('select.sku-feature1 option:selected').selected=false;
								/*$(this).attr("selected", "selected")*/
								$(this).parent().change();
							}
						}
					});
				}, 200);
			}
    	});
    })

    /**
     * /new
     */
    
    //LAZYLOADING
    if ($.fn.lazyLoad) {
    
        var paging = $('.lazyloading-paging');
        if (!paging.length) {
            return;
        }
        // check need to initialize lazy-loading
        var current = paging.find('li.selected');
        if (current.children('a').text() != '1') {
            return;
        }
        paging.hide();
        var win = $(window);
        
        // prevent previous launched lazy-loading
        win.lazyLoad('stop');
        
        // check need to initialize lazy-loading
        var next = current.next();
        if (next.length) {
            win.lazyLoad({
                container: '#main > .content',
                load: function() {
                    win.lazyLoad('sleep');
    
                    var paging = $('.lazyloading-paging').hide();
                    
                    // determine actual current and next item for getting actual url
                    var current = paging.find('li.selected');
                    var next = current.next();
                    var url = next.find('a').attr('href');
                    if (!url) {
                        win.lazyLoad('stop');
                        return;
                    }
    
                    var product_list = $('#product-list .product-list');
                    var loading = paging.parent().find('.loading').parent();
                    if (!loading.length) {
                        loading = $('<div><i class="icon16 loading"></i>Loading...</div>').insertBefore(paging);
                    }
    
                    loading.show();
                    $.get(url, function(html) {
                        var tmp = $('<div></div>').html(html);
                        if ($.Retina) {
                            tmp.find('#product-list .product-list img').retina();
                        }
                        product_list.append(tmp.find('#product-list .product-list').children());
                        var tmp_paging = tmp.find('.lazyloading-paging').hide();
                        paging.replaceWith(tmp_paging);
                        paging = tmp_paging;
                        
                        // check need to stop lazy-loading
                        var current = paging.find('li.selected');
                        var next = current.next();
                        if (next.length) {
                            win.lazyLoad('wake');
                        } else {
                            win.lazyLoad('stop');
                        }
                        
                        loading.hide();
                        tmp.remove();
                    });
                }
            });
        }    
    
    }

});

function showPreorderForm(btn){
	var forms = $('#preorder-modal .wr form'),
		i = 0,
		sku_id = btn.data('sku-id');
	//console.log('sku_id=',sku_id);
	//console.log('forms=',forms);
	if ( forms.size() > 0 ){
		//console.log('size=',forms.size());
		forms.each(function(){
			var id = parseInt($('[name="sku_id"]',this).val()),
				f = $('#shop-preorder-plugin-form-'+id);
			//console.log('id=',id);
			//console.log('f=',f);
			if ( sku_id == id ){
				f.addClass('show').show();
				i = 1;
			} else
				f.hide();
		})
		if ( i ) btn.show();
	}
}

function prepareArcticModal(){
	if ( $('#preorder-modal').size() == 0 ){
		var close_btn = $('<div />').addClass('box-modal_close').addClass('arcticmodal-close').text('Закрыть'),
			box = $('<div />').addClass('box-modal').attr('id','preorder-modal'),
			wr = $('<div />').addClass('wr');
		var d = $('<div />').append( box.append(close_btn).append(wr) ).hide();
		$('body').append(d);
	}
}

function prepareModalPreorderBtn(){
	$('.modal-preorder-btn').unbind('click').click(function(){
		var self = $(this),
			n = self.next().is('.shop-preorder-plugin-form-wr') ? self.next() : 0;
		if ( n )
		$('#preorder-modal').arcticmodal({
			'afterClose' : function(){
				var w = $('#preorder-modal .wr .shop-preorder-plugin-form-wr');
				w.hide();
				self.after(w);
			},
			'beforeOpen' : function(){
				n.show();
				$('#preorder-modal .wr').append(n);
				showPreorderForm(self);
			}
		});
	})
}

function preorderVisibilityCheck(){
	$('.modal-preorder-btn').each(function(){
		var sku_id = $(this).data('sku-id');
		if ( $(this).next().find('#shop-preorder-plugin-form-'+sku_id).size() > 0 )
			$(this).show();
	})
}


var currency_format = function(number, no_html, currency) {
    // Format a number with grouped thousands
    //
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +	 bugfix by: Michael White (http://crestidg.com)

    var i, j, kw, kd, km;
    var decimals = currency.frac_digits;
    var dec_point = currency.decimal_point;
    var thousands_sep = currency.thousands_sep;

    // input sanitation & defaults
    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals && (number - i) ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


    var number = km + kw + kd;
    var s = no_html ? currency.sign : currency.sign_html;
    if (!currency.sign_position) {
        return s + currency.sign_delim + number;
    } else {
        return number + currency.sign_delim + s;
    }
}

var newItem = function($item) {
    return '<li data-id="'+$item.id+'">'+
            '<div class="soaring-cart-img">'+
                '<a href="'+$item.url+'" title="'+$item.name+'">'+
                    '<img src="'+($item.img_url || $.comfortbuy.default_img_url.dummy96)+'" alt="'+$item.name+'">'+
                '</a>'+
            '</div>'+
            '<div class="soaring-cart-name">'+
                '<a href="'+$item.url+'" class="bold">'+$item.name+'</a>&nbsp;<span class="gray">'+(typeof $item.sku_name !== 'undefined' ? $item.sku_name : "")+'</span>'+
            '</div>'+
            '<div class="soaring-cart-price">'+
                '<span class="price nowrap">'+$item.price+'</span>'+//*
            '</div>'+
            '<div class="soaring-cart-quantity">'+
                /*'<a class="soaring-cart-minus" href="javascript:void(0);"><i class="icon-minus-sign"></i></a>'+*/
                '<input type="text" size="3" value="'+$item.cnt+'" class="soaring-cart-qty"> '+$.comfortbuy.locale.pcs+
                /*'<a class="soaring-cart-plus" href="javascript:void(0);"><i class="icon-plus-sign"></i></a>'+*/
            '</div>'+
            '<div class="soaring-cart-delete">'+
                '<a href="javascript:void(0);" class="soaring-cart-del" title="'+$.comfortbuy.locale.remove+'"><i class="icon-remove"></i></a>'+
            '</div>'+
        '</li>';
}

$(document).ready(function () {
    // countdown
    if ($.fn.countdowntimer) {
        $('.js-promo-countdown').each(function () {
            var $this = $(this).html('');
            var id = ($this.attr('id') || 'js-promo-countdown' + ('' + Math.random()).slice(2));
            $this.attr('id', id);
            var start = $this.data('start').replace(/-/g, '/');
            var end = $this.data('end').replace(/-/g, '/');
            $this.countdowntimer({
                startDate: start,
                dateAndTime: end,
                size: 'lg'
            });
        });
    }
});
