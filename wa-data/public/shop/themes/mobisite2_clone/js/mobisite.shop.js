var arrived_sku_count = arrived_sku_count || [];

window.mobisite_site.yaCounterGoals = window.mobisite_site.yaCounterGoals || {};

var mobisite_shop = {

    initDots: function (elems) {

        if (typeof mobisite_site.initDots != "undefined") {

            mobisite_site.initDots(elems);

        }

    },

    windows: (function () {

        if (typeof mobisite_site.windows != "undefined") {

            return mobisite_site.windows;

        }

        else {

            return {};

        }

    })(),

    cookie: (function () {

        var listCookie = function (cookie_name) {

            var cookie = $.cookie(cookie_name);

            if (!cookie) {

                return [];

            }

            else {

                return cookie.split(',');

            }

        };

        var countCookie = function (cookie_name) {

            return listCookie(cookie_name).length;

        };

        var saveCookie = function (cookie_name, list) {

            if (list == null || !list.length) {

                $.cookie(cookie_name, null, { path: '/' });

            }

            else {

                $.cookie(cookie_name, list.join(','), { expires: 30, path: '/' });

            }

        };

        var addCookie = function (product_id, cookie_name) {

            var cookie = listCookie(cookie_name);

            if ($.inArray(product_id + '', cookie) == -1) {

                cookie.push(product_id);

            }

            saveCookie(cookie_name, cookie);

        };

        var removeCookie = function (product_id, cookie_name) {

            var cookie = listCookie(cookie_name);

            var i = $.inArray(product_id + '', cookie);

            if (i != -1) {

                cookie.splice(i, 1)

            }

            saveCookie(cookie_name, cookie);

        };

        return {

            list: listCookie,

            count: countCookie,

            save: saveCookie,

            add: addCookie,

            remove: removeCookie

        }

    })(),

    informerButtonsLabelVisibility: function ($button) {

        if ($button.find("a").attr("data-count") > 0) {

            $button.addClass("informer-button__labeled");

        }

        else {

            $button.removeClass("informer-button__labeled");

        }

    },

    addFavoriteCookie: function (product_id) {

        mobisite_shop.cookie.add(product_id, 'shop_favorite');

    },

    removeFavoriteCookie: function (product_id) {

        mobisite_shop.cookie.remove(product_id, 'shop_favorite');

    },

    addCompareCookie: function (product_id) {

        mobisite_shop.cookie.add(product_id, 'shop_compare');

    },

    removeCompareCookie: function (product_id) {

        mobisite_shop.cookie.remove(product_id, 'shop_compare');

    },

    notification_timeout: '',

    addNotificationPanel: function ($tpl) {

        clearTimeout(mobisite_shop.notification_timeout);

        mobisite_shop.removeNotificationPanel();

        if (typeof $tpl != 'object') {

            $tpl = $($tpl);

        }

        if (!$(".notification-panel").length) {

            $("body").append($tpl.css({top: $(".header-top").height()}));

        }

        mobisite_shop.notification_timeout = setTimeout(function () {

            mobisite_shop.removeNotificationPanel();

        }, 5000);

    },

    removeNotificationPanel: function () {

        $(".notification-panel").remove();

    },

    updateCartCount: function (count) {

        $(".cart-informer a").attr("data-count", count);

        mobisite_shop.informerButtonsLabelVisibility($(".cart-informer"));

    },

    init_lazyloading: function () {

        if ($.fn.lazyLoad) {

            var paging = $('.lazyloading-paging');

            if (!paging.length) {

                return;

            }



            var times = parseInt(paging.data('times'), 10);

            var link_text = paging.data('link-text') || 'Load more';

            var loading_str = paging.data('loading-str') || 'Loading...';



            // check need to initialize lazy-loading

            var current = paging.find('li.selected');

            if (current.children('a').text() != '1') {

                return;

            }

            paging.hide();

            var win = $(window);



            // prevent previous launched lazy-loading

            win.lazyLoad('stop');



            // check need to initialize lazy-loading

            var next = current.next();

            if (next.length) {

                win.lazyLoad({

                    container: '#product-list .product-list',

                    load: function () {

                        win.lazyLoad('sleep');



                        var paging = $('.lazyloading-paging').hide();



                        // determine actual current and next item for getting actual url

                        var current = paging.find('li.selected');

                        var next = current.next();

                        var url = next.find('a').attr('href');

                        if (!url) {

                            win.lazyLoad('stop');

                            return;

                        }



                        var product_list = $('#product-list .product-list');

                        var loading = $(".product-list__loading");

                        if (!loading.length) {

                            loading = $('<div class="product-list__loading">'+loading_str+'</div>').insertBefore(paging);

                        }



                        loading.show();

                        $.get(url, function (html) {

                            var tmp = $('<div></div>').html(html);

                            if (window.$.Retina) {

                                var $list_images = tmp.find('#product-list .product-list img');

                                if (window.mobisite_site.exclude_expansion) {

                                    $list_images = mobisite_site.exclude_expansion($list_images);

                                }

                                $list_images.retina();

                            }

                            product_list.append(tmp.find('#product-list .product-list').children());

                            var tmp_paging = tmp.find('.lazyloading-paging').hide();

                            paging.replaceWith(tmp_paging);

                            paging = tmp_paging;



                            times -= 1;



                            // check need to stop lazy-loading

                            var current = paging.find('li.selected');

                            var next = current.next();

                            if (next.length) {

                                if (!isNaN(times) && times <= 0) {

                                    win.lazyLoad('sleep');

                                    if (!$('.lazyloading-load-more').length) {

                                        $('<a href="#" class="load-more lazyloading-load-more">' + link_text + '</a>').insertAfter(paging)

                                            .click(function () {

                                                loading.show();

                                                times = 1;      // one more time

                                                win.lazyLoad('wake');

                                                win.lazyLoad('force');

                                                /* Интеграция плагина "Наклейки" */

                                                if (window.$.autobadgeFrontend) {

                                                    $.autobadgeFrontend.reinit();

                                                }

                                                return false;

                                            });

                                    }

                                } else {

                                    win.lazyLoad('wake');

                                }

                            } else {

                                win.lazyLoad('stop');

                                $('.lazyloading-load-more').hide();

                            }



                            loading.hide();

                            tmp.remove();

                            /* Интеграция плагина "Наклейки" */

                            if (window.$.autobadgeFrontend) {

                                $.autobadgeFrontend.reinit();

                            }

                        });

                    }

                });

            }

        }

    }

};

$(window).load(function () {

    mobisite_shop.initDots($(".product-list__name"));

    mobisite_shop.initDots($(".home-slider__item-description"));



    if ($('.js-enable-retina').length) {

        var $promo_img = $('.promo img'),

            $list_img = $('.product-list img,.product-info img,.cart img'),

            $bestsellers_img = $('.bestsellers img');

        if (window.mobisite_site.exclude_expansion) {

            $promo_img = mobisite_site.exclude_expansion($promo_img);

            $list_img = mobisite_site.exclude_expansion($list_img);

            $bestsellers_img = mobisite_site.exclude_expansion($bestsellers_img);

        }

        if ($promo_img.length) {

            $promo_img.retina({force_original_dimensions: false});

        }

        if ($list_img.length) {

            $list_img.retina();

        }

        if ($bestsellers_img.length) {

            $bestsellers_img.retina();

        }

    }

});

$(document).ready(function () {

    if (typeof $.fn.slick != "undefined") {

        $(".list-slider").not(".brands-block .list-slider").slick({

            lazyLoad: 'ondemand',

            dots: true,

            arrows: false,

            slidesToShow: 1,

            infinite: true,

            slidesToScroll: 1,

            speed: 300,

            draggable: true,

            swipe: true,

            adaptiveHeight: true,

            pauseOnHover: false,

            focusOnSelect: false,

            touchThreshold: 15

        });

    }



    mobisite_shop.initDots($(".home-slider__item-description"));



    // countdown

    if (typeof $.fn.countdowntimer != "undefined") {

        $('.js-promo-countdown').each(function () {

            var $this = $(this).html('');

            var id = ($this.attr('id') || 'js-promo-countdown' + ('' + Math.random()).slice(2));

            $this.attr('id', id);

            var start = $this.data('start').replace(/-/g, '/');

            var end = $this.data('end').replace(/-/g, '/');

            $this.countdowntimer({

                startDate: start,

                dateAndTime: end,

                size: 'lg'

            });

        });

    }



    $(".main, .reviews").on(device_action, ".js-popup-toggle", function () {

        mobisite_shop.windows.openPopup($(this).closest(".js-expand-wrapper").find(".js-popup"));

    });

    $(document).on(device_action, ".popup-close", function () {

        mobisite_shop.windows.closeAll();

    });

    $(".filter-param__title-text").on(device_action, function () {

        $(this).closest(".filter-param").toggleClass("filter-param_closed");

    });



    $(document).on(device_action, ".list-view", function (e) {

        e.preventDefault();

        var $this = $(this);

        var action = $this.data("action");

        $(".list-view").addClass("active");

        $this.removeClass("active");

        $(".product-list").removeClass("product-list-thumbs product-list-rows").addClass("product-list-"+ action);

        var product_names = $("#product-list").find(".product-list__name");

        if (action == "thumbs") {

            mobisite_shop.initDots(product_names.not(".is-truncated"));

        }

        else {

            product_names.trigger('destroy');

        }

        $.cookie('product_list_view', action, { path: '/' });

        return false;

    });



    $(".load-more").not(".lazyloading-load-more").on(device_action, function (e) {

        e.preventDefault();

        var $this = $(this);

        $this.closest(".product-list").find(".product-list__item_hidden").removeClass("product-list__item_hidden");

        $this.remove();

        return false;

    });



    $(document).on("change", "[name=quantity]", function () {

        var $this = $(this);

        var int_value = parseInt($this.val());

        if ($this.val() <= 0 || isNaN(int_value, 10)) {

            $this.val(1);

        }

        else {

            $this.val(int_value);

        }

    });

    $(document).on(device_action, ".qty-btn", function (e) {

        e.preventDefault();

        var $this = $(this);

        if (!$this.closest("#js-order-page").length) {

            var input = $this.closest(".qty").find("input");

            if ($this.hasClass("qty-plus")) {

                input.val(parseInt(input.val(), 10) + 1).change();

            }

            else {

                input.val(parseInt(input.val(), 10) - 1).change();

            }

        }

    });



    $(".cart-form__quickorder .quickorder-button, .quickorder-button-cart").on(device_action, function () {

        $(".quickorder-form").find("[type=checkbox], [type=radio], select").filter(function () {

            return !$(this).closest(".jq-checkbox, .jq-radio, .jq-selectbox").length;

        }).styler();

    });



    $(document).ajaxSuccess(function (event, xhr, settings) {



        if (settings.url == '/getsearchsmart/') {

            var window_height = $(window).height();

            $result_box = $(".ssearch-result-box");

            var result_box_position_top = $result_box.offset().top;

            var result_box_height = $result_box.css("height", "auto").outerHeight();

            if ((window_height-result_box_position_top) < result_box_height) {

                $result_box.css({height: (window_height-result_box_position_top)});

            } else {

                $result_box.css({height: "auto"});

            }

        }

    });



    // COMPARE

    $(document).on(device_action, ".compare", function () {

        var $this = $(this);

        var is_active = $this.hasClass("active");

        var product_id = $this.data('product');

        if (!is_active) {

            mobisite_shop.addCompareCookie(product_id);

            $this.addClass('active');

            mobisite_shop.addNotificationPanel($("#compare-info").data("success"));

        }

        else {

            mobisite_shop.removeCompareCookie(product_id);

            $this.removeClass('active');

        }

        var shop_compare_count = mobisite_shop.cookie.count("shop_compare");

        $("#compare-info a").attr('data-count', shop_compare_count);

        if (shop_compare_count) {

            $(".topmenu__compare_dynamic").removeClass("topmenu__compare_empty");

        } else {

            $(".topmenu__compare_dynamic").addClass("topmenu__compare_empty");

        }



    });



    //FAVORITE

    $(document).on(device_action, ".favorite", function () {

        var $this = $(this);

        var is_active = $this.hasClass("active");

        var product_id = $this.data('product');

        if (!is_active) {

            mobisite_shop.addFavoriteCookie(product_id);

            $this.addClass('active');

            mobisite_shop.addNotificationPanel($("#favorite-info").data("success"));

        }

        else {

            mobisite_shop.removeFavoriteCookie(product_id);

            $this.removeClass('active');

            if ($this.closest(".action-search").length) {

                $this.closest(".product-list__item").remove();

            }

        }

        var shop_favorite_count = mobisite_shop.cookie.count("shop_favorite");

        $("#favorite-info a").attr('data-count', shop_favorite_count);

        if (shop_favorite_count) {

            $(".topmenu__favorite_dynamic").removeClass("topmenu__favorite_empty");

        } else {

            $(".topmenu__favorite_dynamic").addClass("topmenu__favorite_empty");

        }

    });



    //ADD TO CART

    $(document).on('submit', '.product-list .addtocart.addtocart_as-form', function () {

        var f = $(this);

        var submit = f.find("[type=submit]");

        submit.addClass("btn_loading");

        if (f.data('url')) {

            $(".product-list__popup").remove();

            $.get(f.data('url'), function (html) {

                var $product_tpl = $(f.data("product_tpl"));

                $product_tpl.find(".popup-content").html(html);

                $('body').append($product_tpl);

                submit.removeClass("btn_loading");

                if (typeof mobisite_site != "undefined") {

                    mobisite_site.styler($product_tpl.find('input, select'));

                }

                mobisite_shop.windows.openPopup($product_tpl);

            }, 'html');

            return false;

        }

        $.post(f.attr('action') + '?html=1', f.serialize(), function (response) {

            if (response.status == 'ok') {

                if ($(".cart-summary-page").length) {

                    location.href = '';

                } else {

                    mobisite_shop.addNotificationPanel(f.data("success"));

                    mobisite_shop.updateCartCount(response.data.count);

                    window.mobisite_site.reachGoal(window.mobisite_site.yaCounterGoals.addtocart);

                }

            } else if (response.status == 'fail') {

                alert(response.errors);

            }

            submit.removeClass("btn_loading");

        }, "json");

        return false;

    });

    $(document).on('click', '.product-list .addtocart.addtocart_as-not-form', function () {

        var f = $(this);

        $.post(f.attr('action') + '?html=1', {product_id: f.find("[name=product_id]").val()}, function (response) {

            if (response.status == 'ok') {

                location.reload(true);

            } else if (response.status == 'fail') {

                alert(response.errors);

            }

        });

    });



    $(document).on(device_action, ".notification-panel__close", function (e) {

        e.preventDefault();

        mobisite_shop.removeNotificationPanel();

        return false;

    });

    $(document).on(device_action, ".plugin_arrived-button a", function (e) {

        if (typeof mobisite_site.styler != "undefined") {

            var $this = $(this);

            setTimeout(function () {

                $(".plugin_arrived-popup select").each(function () {

                    if (!$(this).closest(".jq-selectbox").length) {

                        mobisite_site.styler($(this));

                    }

                });

            }, 100);

        }

    });



    (function () {

        if ($.fn.styler != "undefined") {

            setTimeout(function () {

                var $selected_option = $(".list-sorting option[selected=selected]");

                if ($(".list-sorting .jq-selectbox__select-text").text() == "" && $selected_option.length) {

                    $selected_option.removeAttr("selected").attr("selected", "selected").closest("select").styler('destroy').styler().trigger('refresh');

                }

            }, 400);

        }

    })();



    if ($('.buy1step-page').length) {

        $('.buy1step-page [name=user_type]').on('change', function () {

            var $checkbox = $(this),

                $auth_wrapper = $('.buy1step-page .buy1step-auth__form-box');

            if ($checkbox.val() == 1) {

                $auth_wrapper.show();

                $auth_wrapper.find('input').removeAttr('disabled');

            } else {

                $auth_wrapper.hide();

            }

        });

    }



    //PRODUCT FILTERING

    var f = function () {

        var self = this;

        this.f = $(".filters form");

        this.product_list = $('#product-list');

        this.getUrl = function (f) {

            var fields = self.f.serializeArray();

            var params = [];

            for (var i = 0; i < fields.length; i++) {

                if (fields[i].value !== '') {

                    params.push(fields[i].name + '=' + fields[i].value);

                }

            }

            return '?' + params.join('&');

        };

        this.updateProductsCount = function (html) {

            var tmp = $('<div></div>').html(html);

            var products_count = tmp.find(".filter-submit").data("count");

            $(".filter__count").text(products_count);

            var filter_submit = self.f.find(".filter-submit");

            if (products_count > 0) {

                filter_submit.removeClass("disabled");

                mobisite_shop.initDots($(".product-list__name"));

            }

            else {

                filter_submit.addClass("disabled");

            }

        };

        this.onChange = function () {

            var url = self.getUrl();

            var ajax_url = (url == "?") ? '' : url+'&_=_';

            $(window).lazyLoad && $(window).lazyLoad('sleep');

            $.get(ajax_url, function(html) {

                var tmp = $('<div></div>').html(html);

                self.product_list.html(tmp.find('#product-list').html());

                $(".filter__count_hidden").removeClass("filter__count_hidden");

                if ($.fn.styler != "undefined") {

                    self.product_list.find(".list-sorting select").styler();

                }

                self.updateProductsCount(html);

                if (!!(history.pushState && history.state !== undefined)) {

                    window.history.pushState({}, '', url);

                }

                mobisite_shop.init_lazyloading();

                $(window).lazyLoad && $(window).lazyLoad('reload');

                /* Интеграция плагина "Наклейки" */

                if (window.$.autobadgeFrontend) {

                    $.autobadgeFrontend.reinit();

                }

                /* Для плагина smartfilters */

                setTimeout(function () {

                    $(".filters form input").trigger('refresh');

                }, 200);

            });

        };



        $('.filters form input').change(function () {

            self.onChange();

        });

        $('.filters form').submit(function (e) {

            e.preventDefault();

            mobisite_shop.windows.closeAll();

            return false;

        });

        $(".filter-reset").on(device_action, function () {

            $(':input', self.f)

                .not(':button, :submit, :reset, :hidden')

                .removeAttr('checked')

                .removeAttr('selected');

                $(':input[type="text"]', self.f).val('');



            var url = self.f.attr('action');

            $(window).lazyLoad && $(window).lazyLoad('sleep');

            $.get(url+'?_=_', function(html) {

                var tmp = $('<div></div>').html(html);

                var count = tmp.find(".product-list").data("count");

                if (!count) {count = 0;}

                self.product_list.html(tmp.find('#product-list').html());

                self.updateProductsCount(html);

                if (!!(history.pushState && history.state !== undefined)) {

                    window.history.pushState({}, '', url);

                }

                $(window).lazyLoad && $(window).lazyLoad('reload');

                /* Интеграция плагина "Наклейки" */

                if (window.$.autobadgeFrontend) {

                    $.autobadgeFrontend.reinit();

                }

            });

            setTimeout(function () {

                self.f.find("input").trigger("refresh");

            }, 100);

        });

    };

    f();



    //LAZYLOADING

    mobisite_shop.init_lazyloading();



});



$(window).load(function () {

    $(".checkout input").trigger('refresh');

});