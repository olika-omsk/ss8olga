var compare_page = {
    compare_sliders: $(".compare-slider__items"),
    refresh: function ($this) {
        if (typeof $.fn.slick == "undefined") return false;
        var $slider_side = $this.closest(".compare-slider__side");
        var col = $slider_side.data("col");
        var $next_slide;
        var $slide_index;
        var $compare_slides = $this.slick('getSlick')['$slides'];
        $compare_slides.each(function (i) {
            if ($(this).hasClass("slick-active")) {
                $next_slide = $(this);
                $slide_index = i;
                return false;
            }

        });
        if ($next_slide) {
            var product_id = $next_slide.data("id");
            var $table_side = $(".compare-table__side-"+ col);
            var $current_page = $slider_side.find(".compare-slider-pages__current");
            $current_page.text($slide_index + 1);
            $table_side.find(".compare-table__features").removeClass("active");
            $table_side.find(".compare-table__features-"+product_id).addClass("active");
            
            $slider_side.find(".compare-slider-pages__total").text($compare_slides.length);
            if ($current_page.text() > $compare_slides.length) {
                $current_page.text($compare_slides.length);
            }
        }
    },
    fix_products: function () {
        var self = this;
        this.getScrollTop = function () {
            return $(window).scrollTop();
        }
        this.compare_slider = $(".compare-slider");
        this.wrapper = this.compare_slider.closest(".compare-slider-wrapper");
        this.wrapper_height = self.wrapper.outerHeight();
        self.wrapper.css({height: self.wrapper_height});
        this.compare_slider_top = self.wrapper.offset().top;
        this.is_fixed = self.compare_slider.hasClass("fixed");
        this.get_header_height = function () {
            return $(".header-top").height();
        }
        this.fixedMethod = function () {
            var scrollTop = self.getScrollTop() + self.get_header_height();
            if (scrollTop > self.compare_slider_top && !self.is_fixed) {
                self.compare_slider.addClass("fixed").css({top: self.get_header_height(), height: 'auto'});
                compare_page.compare_sliders.slick('setPosition');
                self.is_fixed = true;
            }
            else if (scrollTop <= self.compare_slider_top && self.is_fixed) {
                self.compare_slider.removeClass("fixed").css({top: 'auto', height: self.wrapper.height()});
                compare_page.compare_sliders.slick('setPosition');
                self.is_fixed = false;
            }
        }
        self.fixedMethod();

        $(document).scroll(function () {
            scrollTop = self.getScrollTop();
            self.is_fixed = self.compare_slider.hasClass("fixed");
            self.fixedMethod();
        });
    },
    compare_redirect: function () {
        location.href = location.href.replace(/compare\/.*/, 'compare/');
    }
}
$(document).ready(function () {   
    if (typeof $.fn.slick != "undefined") {
        compare_page.compare_sliders.on("reInit", function(e, slick, currentSlide, nextSlide) {
            var $this = $(e.target);
        });
        compare_page.compare_sliders.on("afterChange", function(e, slick) {
            var $this = $(e.target);
            compare_page.refresh($this);
        });
        compare_page.compare_sliders.slick({
            dots: false,
            arrows: false,
            slidesToShow: 1,
            infinite: false,
            slidesToScroll: 1,
            speed: 300,
            draggable: true,
            swipe: true,
            adaptiveHeight: true,
            pauseOnHover: false,
            focusOnSelect: false,
            touchThreshold: 15
        });

        var timer_iteration = 0;
        var timer = setInterval(function () {
            if (compare_page.compare_sliders.find(".slick-slide").length) {
                $(".compare-slider__side").each(function () {
                    var $this = $(this);
                    if ($this.data("col") != 1) {
                        $this.find(".compare-slider__items").slick("slickGoTo", 1, true);
                    }
                });
                clearInterval(timer);
            }
            if (timer_iteration > 100) {
                clearInterval(timer);
            }
            timer_iteration++;
        }, 50);
    }
    if (typeof device_action != "undefined") {
        $(".compare-slider__remove").on(device_action, function () {
            var $this = $(this);
            var compare = $.cookie('shop_compare');
            if (compare) {
                compare = compare.split(',');
            } else {
                compare = [];
            }
            var id = $this.closest(".compare-slider__side").find(".slick-active").data("id");
            var i = $.inArray(id + '', compare);
            if (i != -1) {
                compare.splice(i, 1)
            }
            if (compare.length) {
                $.cookie('shop_compare', compare.join(','), { expires: 30, path: '/'});
            } else {
                $.cookie('shop_compare', null, {path: '/'});
            }

            var href_ids = location.href.match(/[0-9,]+/)[0].split(',');
            var ind = $.inArray(id + '', href_ids);
            if (ind != -1) {
                href_ids.splice(ind, 1)
            }
            if (href_ids.length == 0) {
                compare_page.compare_redirect();
                return false;
            } else {
                href_ids = href_ids.join(',');
                var url = location.href.replace(/compare\/.[0-9,]+/, 'compare/' + href_ids);
                window.history.pushState(null, null, url);
            }

            var current_slide = $this.closest(".compare-slider__side").find(".compare-slider__items").slick('slickCurrentSlide');
            compare_page.compare_sliders.each(function () {
                var $t = $(this);
                $t.slick('slickRemove',current_slide+1, true);
                compare_page.refresh($t);
            });

        });
    }

    $("[name=compare_diff]").change(function () {
        if ($(this).val() == "diff") {
            $(".compare-table .compare-table__item_same").hide();
        } else {
            $(".compare-table .compare-table__item_same").show();
        }
    });

    if (typeof device_action != "undefined") {
        $('#compare-clear').on(device_action, function () {
            $.cookie('shop_compare', null, {path: '/'});
            compare_page.compare_redirect();
        });
    }
    compare_page.fix_products();
});
$(window).load(function () {
    if (typeof mobisite_site.initDots != "undefined") {
        mobisite_site.initDots($(".compare-slider__name"));
    }
});