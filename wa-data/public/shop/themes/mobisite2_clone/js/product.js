function Product(form, options) {
    var self = this;
    this.form = $(form);
    this.add2cart = this.form.find(".add2cart");
    this.button = this.add2cart.find("[type=submit]");
    this.quickorder_button = this.add2cart.find(".storequickorder_product_button");
    this.getQty = function () {
        var qty = self.form.find("[name=quantity]").val();
        if (!qty) {
            qty = 1;
        }
        return qty;
    };
    for (var k in options) {
        this[k] = options[k];
    }
    // add to cart block: services
    this.form.find(".services input[type=checkbox]").change(function () {
        var obj = $('select[name="service_variant[' + $(this).val() + ']"]');
        if (obj.length) {
            if ($(this).is(':checked')) {
                obj.removeAttr('disabled').trigger('refresh');
            } else {
                obj.attr('disabled', 'disabled').trigger('refresh');
            }
        }
        self.cartButtonVisibility(true);
        self.updatePrice();
    });

    this.form.find(".services .service-variants").on('change', function () {
        self.cartButtonVisibility(true);
        self.updatePrice();
    });
    this.form.find('.inline-select a').click(function () {
        var d = $(this).closest('.inline-select');
        d.find('a.selected').removeClass('selected');
        $(this).addClass('selected');
        d.find('.sku-feature').val($(this).data('value')).change().trigger('refresh');
        return false;
    });

    this.form.find(".skus input[type=radio]").change(function () {
        var $this = $(this);
        if ($this.data('image-id') && typeof $.fn.slick !== "undefined" && typeof mobisite_shop.product_gallery != "undefined") {
            mobisite_shop.product_gallery.slick("slickGoTo", $("#product-image-" + $this.data('image-id')).closest(".slick-slide").data("slick-index"), false);
        }
        self.add2cart_visibility(!$this.data('disabled'));
        var sku_id = $this.val();
        self.changeVendorCode($this.data("vendor"));
        self.updateSkuServices(sku_id);
        self.cartButtonVisibility(true);
        self.updatePrice();
    });
    var $initial_cb = this.form.find(".skus input[type=radio]:checked:not(:disabled)");
    if (!$initial_cb.length) {
        $initial_cb = this.form.find(".skus input[type=radio]:not(:disabled):first").prop('checked', true).trigger('refresh').click();
    }
    $initial_cb.click();

    this.getSku = function () {
        var $sku_features = self.form.find(".sku-feature").not(".jq-selectbox");
        if ($sku_features.length) {
            var key = "";
            $sku_features.each(function () {
                key += $(this).data('feature-id') + ':' + $(this).val() + ';';
            });
            return self.features[key];
        }
    };
    this.form.find(".sku-feature").not(".jq-selectbox").change(function () {
        var sku = self.getSku();
        if (sku) {
            if (sku.image_id && typeof $.fn.slick !== "undefined" && typeof mobisite_shop.product_gallery != "undefined") {
                mobisite_shop.product_gallery.slick("slickGoTo", $("#product-image-" + sku.image_id).closest(".slick-slide").data("slick-index"), false);
            }
            self.updateSkuServices(sku.id);
            if (sku.available) {
                self.form.find(".sku-no-stock").hide();
            } else {
                self.form.find("div.stocks > div").hide();
                self.form.find(".sku-no-stock").show();
            }
            self.add2cart_visibility(sku.available);
            self.add2cart.find(".price").data('price', sku.price);
            self.updatePrice(sku.price, sku.compare_price);
        } else {
            self.form.find("div.stocks > div").hide();
            self.form.find(".sku-no-stock").show();
            self.button.attr('disabled', 'disabled').trigger('refresh');
            self.add2cart.find(".compare-at-price").remove();
            self.add2cart.find(".price").empty();
        }
        self.changeVendorCode(sku.vendor);
        self.cartButtonVisibility(true);
    });
    this.form.find(".sku-feature:first").change().trigger('refresh');

    if (!this.form.find(".skus input:radio:checked").length) {
        this.form.find(".skus input:radio:enabled:first").attr('checked', 'checked').trigger('refresh');
    }

    this.form.submit(function () {
        var f = $(this);
        
        var submit = f.find("[type=submit]");
        submit.addClass("btn_loading");

        $.post(f.attr('action') + '?html=1', f.serialize(), function (response) {
            f.find('.adding2cart').hide();
            if (response.status == 'ok') {

                mobisite_shop.addNotificationPanel(f.data("success"));
                mobisite_shop.updateCartCount(response.data.count);
                window.mobisite_site.reachGoal(window.mobisite_site.yaCounterGoals.addtocart);

                if ($("#cart-form-dialog").length) {
                    setTimeout(function () {
                        mobisite_shop.windows.removePopup(f.closest(".js-popup"));
                    }, 100);
                }

                if (response.data.error) {
                    alert(response.data.error);
                }
            } else if (response.status == 'fail') {
                alert(response.errors);
            }
            submit.removeClass("btn_loading");
        }, "json");

        return false;
    });
    (function () {
        $(document).on("change", self.add2cart.find("[name=quantity]"), function () {
            setTimeout(function () {
                if (self.features) {
                    var sku = self.getSku();
                    if (sku) {
                        self.updatePrice(sku.price, sku.compare_price);
                    } else {
                        self.updatePrice();
                    }
                } else if (!self.form.find(".skus input[type=radio]").length) {
                    var $price = self.form.find(".price");
                    if ($price.length) {
                        self.updatePrice(self.form.find(".price").data("price"), $price.closest(".add2cart__pricing").data("compare_price"));
                    }
                } else {
                    self.updatePrice();
                }
            }, 10);
        });
    })();
}
Product.prototype.getEscapedText = function( bad_string ) {
    return $("<div>").text( bad_string ).html();
};

Product.prototype.currencyFormat = function (number, no_html) {
    // Format a number with grouped thousands
    //
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +	 bugfix by: Michael White (http://crestidg.com)

    var i, j, kw, kd, km;
    var decimals = this.currency.frac_digits;
    var dec_point = this.currency.decimal_point;
    var thousands_sep = this.currency.thousands_sep;

    // input sanitation & defaults
    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals && (number - i) ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


    var number = km + kw + kd;
    var s = no_html ? this.currency.sign : this.currency.sign_html;
    if (!this.currency.sign_position) {
        return s + this.currency.sign_delim + number;
    } else {
        return number + this.currency.sign_delim + s;
    }
};


Product.prototype.serviceVariantHtml= function (id, name, price) {
    return $('<option data-price="' + price + '" value="' + id + '"></option>').text(name + ' (+' + this.currencyFormat(price, 1) + ')');
};

Product.prototype.updateSkuServices = function (sku_id) {
    this.form.find("div.stocks > div").hide();
    this.form.find(".sku-" + sku_id + "-stock").show();
    for (var service_id in this.services[sku_id]) {
        var v = this.services[sku_id][service_id];
        if (v === false) {
            this.form.find(".service-" + service_id).hide().find('input,select').attr('disabled', 'disabled').removeAttr('checked').trigger('refresh');
        } else {
            this.form.find(".service-" + service_id).show().find('input').removeAttr('disabled').trigger('refresh');
            if (typeof (v) == 'string') {
                this.form.find(".service-" + service_id + ' .service-price').html(this.currencyFormat(v));
                this.form.find(".service-" + service_id + ' input').data('price', v);
            } else {
                var select = this.form.find(".service-" + service_id + ' .service-variants');
                var selected_variant_id = select.val();
                for (var variant_id in v) {
                    var obj = select.find('option[value=' + variant_id + ']');
                    if (v[variant_id] === false) {
                        obj.hide();
                        if (obj.attr('value') == selected_variant_id) {
                            selected_variant_id = false;
                        }
                    } else {
                        if (!selected_variant_id) {
                            selected_variant_id = variant_id;
                        }
                        obj.replaceWith(this.serviceVariantHtml(variant_id, v[variant_id][0], v[variant_id][1]));
                    }
                }
                this.form.find(".service-" + service_id + ' .service-variants').val(selected_variant_id).trigger('refresh');
            }
        }
    }
};
Product.prototype.updatePrice = function (price, compare_price) {
    var qty = this.getQty();
    if (price === undefined) {
        var input_checked = this.form.find(".skus input:radio:checked");
        if (input_checked.length) {
            var price = parseFloat(input_checked.data('price'));
            var compare_price = parseFloat(input_checked.data('compare-price'));
        } else {
            var price = parseFloat(this.add2cart.find(".price").data('price'));
        }
    }
    if (compare_price) {
        if (!this.add2cart.find(".compare-at-price").length) {
            var $price = this.add2cart.find(".price");
            $price.after($price.data("compare_tpl"));
        }
        this.add2cart.find(".compare-at-price__val").html(this.currencyFormat(compare_price * qty)).show();
    } else {
        this.add2cart.find(".compare-at-price").remove();
    }
    var self = this;

    $active_services = this.form.find(".services input:checked");
    $active_services.each(function () {
        var s = $(this).val();
        if (self.form.find('.service-' + s + '  .service-variants').length) {
            price += parseFloat(self.form.find('.service-' + s + '  .service-variants :selected').data('price'));
        } else {
            price += parseFloat($(this).data('price'));
        }
    });
    this.add2cart.find(".price").html(this.currencyFormat(price * qty));
    if (qty == 1 && !$active_services.length) {
        this.add2cart.find(".compare-at-price").show();
    }
    else {
        this.add2cart.find(".compare-at-price").hide();
    }
}

Product.prototype.cartButtonVisibility = function (visible) {
    //toggles "Add to cart" / "%s is now in your shopping cart" visibility status
    if (visible) {
        this.add2cart.find('.compare-at-price').show();
        this.add2cart.find('input[type="submit"]').show();
        this.add2cart.find('.price').show();
        this.add2cart.find('.qty').show();
        this.add2cart.find('span.added2cart').hide();
    } else {
        this.add2cart.find('.compare-at-price').remove();
        this.add2cart.find('input[type="submit"]').hide();
        this.add2cart.find('.price').hide();
        this.add2cart.find('.qty').hide();
        this.add2cart.find('span.added2cart').show();
    }
}

Product.prototype.changeVendorCode = function (vendor_code) {
    if (vendor_code) {
        $(".product-vendor__code").html(vendor_code).closest(".product-vendor").show();
    }
    else {
        $(".product-vendor").hide();
    }
}

Product.prototype.add2cart_visibility = function (is_available) {
    var $add2cart_main = this.add2cart.find(".add2cart__main");
    var qty = this.getQty();
    if (is_available) {
        $add2cart_main.html(this.add2cart.data("available"));
        this.add2cart.removeClass("add2cart_unavailable").addClass("add2cart_available");
    } else {
        $add2cart_main.html(this.add2cart.data("unavailable"));
        this.add2cart.removeClass("add2cart_available").addClass("add2cart_unavailable");
    }
    $add2cart_main.find("[name=quantity]").val(qty).change();
}