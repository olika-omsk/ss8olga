$(document).ready(function () {
    mobisite_shop.product_gallery = mobisite_shop.product_gallery || $("#product-gallery.product-gallery-slider");
    if (typeof $.fn.slick !== "undefined") {
        mobisite_shop.product_gallery.slick({
            dots: true,
            arrows: false,
            slidesToShow: 1,
            infinite: true,
            slidesToScroll: 1,
            speed: 300,
            draggable: false,
            swipe: false,
            adaptiveHeight: true,
            pauseOnHover: false,
            focusOnSelect: false,
            touchThreshold: 15,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        draggable: true,
                        swipe: true
                    }
                }
            ]
        });
    }
    $(".product-tabs .tabs-menu__link").click(function (e) {
        var $this = $(this);
        if (!$this.hasClass("tabs-menu__link_revent-expand")) {
            $this.closest(".product-tabs__item").toggleClass("active");
        }
    });

    var gallery;

    $(document).on("click touchend", "#pswp", function (e) {
        if ($(e.target).hasClass('pswp__zoom-wrap')) {
            gallery.close();
        }
    });

    var $coreImages = $("#product-gallery a").not(".slick-cloned a");
    $coreImages.on("click", function(e) {
        e.preventDefault();
        var $product_gallery = $("#product-gallery");
        var slick_index = $(this).closest(".image").data("slick-index");
        var index =  slick_index ? slick_index : 0;
        var options = {
            index: index,
            history: false,
            fullscreenEl: false,
            zoomEl: true,
            shareEl: false,
            closeOnVerticalDrag: false,
            maxSpreadZoom: 10
        };
        var items = [];
        var window_width = window.innerWidth;
        var window_height = window.innerHeight;
        $coreImages.each(function (index, n) {
            var $this = $(this);
            items[index] = {
                src: $this.attr("href"),
                w: window_width,
                h: window_height
            }
        });
        if (!$(".pswp").length) {
            $("body").append($($product_gallery.data("swiper")));
        }
        var pswpElement = document.querySelectorAll('.pswp')[0];
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
        return false;
    });
});