$(function () {

    function updateCart(data)
    {
        $(".cart-total").html(data.total);
        $(".cart-informer a").attr("data-count", data.count);
        mobisite_shop.informerButtonsLabelVisibility($(".cart-informer"));
        if (data.discount_numeric) {
            $(".cart-discount").closest('.row').show();
        }
        $(".cart-discount").html('&minus; ' + data.discount);
        
        if (data.add_affiliate_bonus) {
            $(".affiliate").show().html(data.add_affiliate_bonus);
        } else {
            $(".affiliate").hide();
        }

        if (data.affiliate_discount) {
            $('.affiliate-discount-available').html(data.affiliate_discount);
            if ($('.affiliate-discount').data('use')) {
                $('.affiliate-discount').html('&minus; ' + data.affiliate_discount);
            }
        }
    }

    $(".cart a.delete").click(function () {
        var row = $(this).closest('.row');
        $.post('delete/', {html: 1, id: row.data('id')}, function (response) {
            if (response.data.count == 0) {
                location.reload();
            }
            row.remove();
            updateCart(response.data);
        }, "json");
        return false;
    });

    $(".cart .qty input").change(function () {
        var that = $(this);
        if (that.val() > 0) {
            var row = that.closest('.row');
            if (that.val()) {
                $.post('save/', {html: 1, id: row.data('id'), quantity: that.val()}, function (response) {
                    row.find('.item-total').html(response.data.item_total);
                    if (response.data.q) {
                        that.val(response.data.q).trigger("refresh");
                    }
                    if (response.data.error) {
                        alert(response.data.error);
                    } else {
                        that.removeClass('error');
                    }
                    updateCart(response.data);
                }, "json");
            }
        } else {
            that.val(1).trigger("refresh");
        }
    });

    $(".cart .services input:checkbox").change(function () {
        var obj = $('select[name="service_variant[' + $(this).closest('.row').data('id') + '][' + $(this).val() + ']"]');
        if (obj.length) {
            if ($(this).is(':checked')) {
                obj.removeAttr('disabled').trigger("refresh");
            } else {
                obj.attr('disabled', 'disabled').trigger("refresh");
            }
        }

        var div = $(this).closest('.services__item');
        var row = $(this).closest('.row');
        if ($(this).is(':checked')) {
           var parent_id = row.data('id')
           var data = {html: 1, parent_id: parent_id, service_id: $(this).val()};
           var $variants = $('[name="service_variant[' + parent_id + '][' + $(this).val() + ']"]');
           if ($variants.length) {
               data['service_variant_id'] = $variants.val();
           }
           $.post('add/', data, function(response) {
               div.data('id', response.data.id);
               row.find('.item-total').html(response.data.item_total);
               updateCart(response.data);
           }, "json");
        } else {
           $.post('delete/', {html: 1, id: div.data('id')}, function (response) {
               div.data('id', null);
               row.find('.item-total').html(response.data.item_total);
               updateCart(response.data);
           }, "json");
        }
    });

    $(".cart .services select").change(function () {
        var row = $(this).closest('.row');
        $.post('save/', {html: 1, id: $(this).closest('.services__item').data('id'), 'service_variant_id': $(this).val()}, function (response) {
            row.find('.item-total').html(response.data.item_total);
            updateCart(response.data);
        }, "json");
    });

    $("#cancel-affiliate").click(function () {
        $(this).closest('form').append('<input type="hidden" name="use_affiliate" value="0">').submit();
        return false;
    });

    // listen coupon input value for change and hide its error message if changing have been happened

    var onInputChange = function ($input, onChange) {
        var prev_input_val = $input.val() || '';
        var timer_id, timeout = 500;
        $input.keydown(function () {
            if (timer_id) {
                clearTimeout(timer_id);
            }
            timer_id = setTimeout(function () {
                if (prev_input_val !== $input.val()) {
                    onChange.apply($input, arguments);
                }
                prev_input_val = $input.val();
            }, timeout);
        }).change(onChange).trigger("refresh");
    };

    onInputChange($('#apply-coupon-code input[type="text"]'), function () {
        $(this).closest(".coupon-code__text").removeClass("coupon-code__text_error");
    });

    $(".cart-items .qty input").trigger('change');
    
    console.log('CART!!!!');

});