$(document).ready(function () {

    //CART dialog for multi-SKU products
    $('.dialog').on('click', 'a.dialog-close', function () {
        $(this).closest('.dialog').hide().find('.cart').empty();
        return false;
    });
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $(".dialog:visible").hide().find('.cart').empty();
        }
    });


    // Вид списка товаров
    $(".container").on('click', '.sort_view_wrap .view > div', function () {
        var old_view = $.cookie('product_list_view_mobile'),
            new_view = $(this).data('view');

        if (!old_view) {
            old_view = $('.product-list').data('default-view');
        };

        if (new_view != old_view) {
            $('.sort_view_wrap .view > div').removeClass('active');
            $(this).addClass('active');
            $('.product-list').removeClass(old_view);
            $('.product-list').addClass(new_view);

            if (new_view == 'two_blocks') {
                align_height($('.two_blocks .prod_info .all'));
            };

            $.cookie('product_list_view_mobile', new_view, { expires: 30, path: '/'});
        };

        return false;
    });

    align_height($('.two_blocks .prod_info .all'));


    //Открытие и скрытие выпадающего списка сортировки
    $(".sorting_active").click(function () {
        var list_sort = $(this).siblings('.sorting_list'),
            isHide = list_sort.css('display') == 'none';

        if (isHide) {
            list_sort.show();
            var yourClick = true;
            $(document).bind('click.myEvent', function (e) {
                if (!$(e.target).hasClass('sorting_active') && !yourClick && $(e.target).closest('#cat_product_sort').length == 0) {
                    $(".sorting_list").hide();
                    $(document).unbind('click.myEvent');
                }
                yourClick = false;
            });
        } else {
            list_sort.hide();
        }
    });


    //ADD TO CART
    $(".container").on('submit', '.product-list form.addtocart', function () {
        var f = $(this);
        f.find('.adding2cart').addClass('icon16 loading').show();
        if (f.data('url')) {
            var d = $('#dialog');
            var c = d.find('.cart');
            c.load(f.data('url'), function () {
                f.find('.adding2cart').hide();
                c.prepend('<a href="#" class="dialog-close">&times;</a>');
                d.show();
                if ((c.height() > c.find('form').height())) {
                    c.css('bottom', 'auto');
                } else {
                    c.css('bottom', '15%');
                }
            });
            return false;
        }
        $.post(f.attr('action') + '?html=1', f.serialize(), function (response) {
            f.find('.adding2cart').hide();

            if (response.status == 'ok') {

                var cart_total = $(".cart-count");
                cart_total.closest('#cart').removeClass('empty');

                f.find('button[type="submit"]').hide();
                f.find('.price').hide();
                f.find('span.added2cart').show();
                cart_total.html(response.data.count);
                $('#cart-content').append($('<div class="cart-just-added"></div>').html(f.find('span.added2cart').text()));

            } else if (response.status == 'fail') {
                alert(response.errors);
            }

        }, "json");
        return false;
    });


    $('.filters_title').toggle(function() {
        $(this).find('i.fa').removeClass('fa-caret-right').addClass('fa-caret-down');
        $('.filters').show();
    }, function() {
        $(this).find('i.fa').removeClass('fa-caret-down').addClass('fa-caret-right');
        $('.filters').hide();
    });

    $('#brand-page .sub-categories a').addClass('main_title');



    //PRODUCT FILTERING
    var f = function () {

        var ajax_form_callback = function (f) {
            var fields = f.serializeArray();
            var params = [];
            for (var i = 0; i < fields.length; i++) {
                if (fields[i].value !== '') {
                    params.push(fields[i].name + '=' + fields[i].value);
                }
            }
            var url = '?' + params.join('&');
            $(window).lazyLoad && $(window).lazyLoad('sleep');
            $('#product-list').html('<img src="' + f.data('loading') + '">');
            $.get(url+'&_=_', function(html) {
                var tmp = $('<div></div>').html(html);
                $('#product-list').html(tmp.find('#product-list').html());
                if (!!(history.pushState && history.state !== undefined)) {
                    window.history.pushState({}, '', url);
                }
                $(window).lazyLoad && $(window).lazyLoad('reload');
            });
        };

        $('.filters.ajax form input').change(function () {
            ajax_form_callback($(this).closest('form'));
        });
        $('.filters.ajax form').submit(function () {
            ajax_form_callback($(this));
            return false;
        });

    };
    f();


    //LAZYLOADING
    if ($.fn.lazyLoad) {
        var paging = $('.lazyloading-paging');
        if (!paging.length) {
            return;
        }

        var times = parseInt(paging.data('times'), 10);
        var link_text = paging.data('linkText') || 'Load more';
        var loading_str = paging.data('loading-str') || 'Loading...';

        // check need to initialize lazy-loading
        var current = paging.find('li.selected');
        if (current.children('a').text() != '1') {
            return;
        }
        paging.hide();
        var win = $(window);

        // prevent previous launched lazy-loading
        win.lazyLoad('stop');

        // check need to initialize lazy-loading
        var next = current.next();
        if (next.length) {
            win.lazyLoad({
                container: '#product-list .product-list',
                load: function () {
                    win.lazyLoad('sleep');

                    var paging = $('.lazyloading-paging').hide();

                    // determine actual current and next item for getting actual url
                    var current = paging.find('li.selected');
                    var next = current.next();
                    var url = next.find('a').attr('href');
                    if (!url) {
                        win.lazyLoad('stop');
                        return;
                    }

                    var product_list = $('#product-list .product-list');
                    var loading = paging.parent().find('.loading').parent();
                    if (!loading.length) {
                        loading = $('<div><i class="icon16 loading"></i>'+loading_str+'</div>').insertBefore(paging);
                    }

                    loading.show();
                    $.get(url, function (html) {
                        var tmp = $('<div></div>').html(html);
                        if ($.Retina) {
                            tmp.find('#product-list .product-list img').retina();
                        }
                        product_list.append(tmp.find('#product-list .product-list').children());
                        var tmp_paging = tmp.find('.lazyloading-paging').hide();
                        paging.replaceWith(tmp_paging);
                        paging = tmp_paging;

                        times -= 1;

                        // check need to stop lazy-loading
                        var current = paging.find('li.selected');
                        var next = current.next();
                        if (next.length) {
                            if (!isNaN(times) && times <= 0) {
                                win.lazyLoad('sleep');
                                if (!$('.lazyloading-load-more').length) {
                                    $('<a href="#" class="lazyloading-load-more">' + link_text + '</a>').insertAfter(paging)
                                        .click(function () {
                                            loading.show();
                                            times = 1;      // one more time
                                            win.lazyLoad('wake');
                                            win.lazyLoad('force');
                                            return false;
                                        });
                                }
                            } else {
                                win.lazyLoad('wake');
                            }
                        } else {
                            win.lazyLoad('stop');
                            $('.lazyloading-load-more').hide();
                        }

                        loading.hide();
                        tmp.remove();
                    });
                }
            });
        }
    }

});