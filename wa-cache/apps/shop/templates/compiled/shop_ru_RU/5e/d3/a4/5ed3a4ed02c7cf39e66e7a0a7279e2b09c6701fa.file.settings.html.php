<?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:16
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/settings.html" */ ?>
<?php /*%%SmartyHeaderCode:1966520845eb29fc854d535-98483591%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5ed3a4ed02c7cf39e66e7a0a7279e2b09c6701fa' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/settings.html',
      1 => 1566472736,
      2 => 'file',
    ),
    'c91782024797658fec8f898fe820eb689d310026' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/include.settings_basic_settings.html',
      1 => 1568104307,
      2 => 'file',
    ),
    'fd42bfec54079136065dd8cc934d6db166803b84' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/include.settings_available_shipping.html',
      1 => 1567695712,
      2 => 'file',
    ),
    '832af43c89cf5cbb373dfb701e897a288da05349' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/include.settings_shipping_cost.html',
      1 => 1568823164,
      2 => 'file',
    ),
    '1123a163fdd7d88e70044cb9e444897fa3ddabdd' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/include.settings_shipping_options.html',
      1 => 1566818016,
      2 => 'file',
    ),
    '5bf69227653d90c9087cf6bd5f2c5b917d78ac0d' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/include.settings_integration.html',
      1 => 1566818016,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1966520845eb29fc854d535-98483591',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'wa_url' => 0,
    'wa' => 0,
    'points_for_parcel' => 0,
    'settings' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5eb29fc85ee936_70491720',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5eb29fc85ee936_70491720')) {function content_5eb29fc85ee936_70491720($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['wa_url']->value;?>
wa-plugins/shipping/boxberry/js/settings.js?<?php echo $_smarty_tpl->tpl_vars['wa']->value->version(true);?>
"></script>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['wa_url']->value;?>
wa-plugins/shipping/boxberry/css/boxberry.css?<?php echo $_smarty_tpl->tpl_vars['wa']->value->version(true);?>
">

<div id="js-shipping-boxberry-settings">
    
    <?php /*  Call merged included template "./include.settings_basic_settings.html" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('./include.settings_basic_settings.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '1966520845eb29fc854d535-98483591');
content_5eb29fc8554b19_72827870($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "./include.settings_basic_settings.html" */?>

    
    <?php /*  Call merged included template "./include.settings_available_shipping.html" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('./include.settings_available_shipping.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '1966520845eb29fc854d535-98483591');
content_5eb29fc858c847_64172475($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "./include.settings_available_shipping.html" */?>

    
    <?php /*  Call merged included template "./include.settings_shipping_cost.html" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('./include.settings_shipping_cost.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '1966520845eb29fc854d535-98483591');
content_5eb29fc85b42c0_94306797($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "./include.settings_shipping_cost.html" */?>

    
    <?php /*  Call merged included template "./include.settings_shipping_options.html" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('./include.settings_shipping_options.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '1966520845eb29fc854d535-98483591');
content_5eb29fc85c45c2_64771581($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "./include.settings_shipping_options.html" */?>

    
    <?php /*  Call merged included template "./include.settings_integration.html" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('./include.settings_integration.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '1966520845eb29fc854d535-98483591');
content_5eb29fc85dae61_10223421($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "./include.settings_integration.html" */?>
</div>

<script>
    (function ($) {
        new systemShippingBoxberryPluginSettings({
            $wrapper: $("#js-shipping-boxberry-settings"),
            points_for_parcel: <?php echo json_encode($_smarty_tpl->tpl_vars['points_for_parcel']->value);?>
,
            saved_token: <?php echo json_encode(ifset($_smarty_tpl->tpl_vars['settings']->value,'token',''));?>
,
        });
    })(jQuery);
</script><?php }} ?><?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:16
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/include.settings_basic_settings.html" */ ?>
<?php if ($_valid && !is_callable('content_5eb29fc8554b19_72827870')) {function content_5eb29fc8554b19_72827870($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['token'] = new Smarty_variable(ifset($_smarty_tpl->tpl_vars['settings']->value,'token',''), null, 0);?>
<div class="field-group">
    <div class="field">
        <div class="name"></div>
        <div class="value"><h3><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Basic settings');?>
</h3></div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Service name');?>
</div>
        <div class="value">
            <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[service]" value="<?php echo ifset($_smarty_tpl->tpl_vars['settings']->value,'service','');?>
">
        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('API URL');?>
</div>
        <div class="value">
            <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[api_url]" value="<?php echo ifset($_smarty_tpl->tpl_vars['settings']->value,'api_url','');?>
">
            <br>
            <span class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('To switch to the test mode, request a test API address and token from Boxberry support team.');?>
</span>
        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Token');?>
</div>
        <div class="value">
            <input type="text" class="js-boxberry-token" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[token]" value="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" required>
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Parcel office city');?>
</div>
        <div class="value">
            <input class="ui-autocomplete-input js-boxberry-targetstart-autocomplete" autocomplete="off" value="<?php echo $_smarty_tpl->tpl_vars['points_by_settings']->value['city'];?>
"
                   placeholder="<?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Start typing a city name');?>
" <?php if (!$_smarty_tpl->tpl_vars['token']->value){?>style="display: none"<?php }?>>
            <span class="s-js-boxberry-targetstart-start-message">
                <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('To select a parcel office, please enter a token and save the settings.');?>

            </span>
            <br>
            <span class="errormsg s-js-boxberry-targetstart-autocomplete">
                <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('There are no parcel offices in your city.');?>

            </span>
        </div>
    </div>

    <div class="field js-boxberry-parcel-points-wrapper" <?php if (!$_smarty_tpl->tpl_vars['points_by_settings']->value['targetstart']){?>style="display: none"<?php }?>>
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Parcel office');?>
</div>
        <div class="value no-shift">
            <select name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[targetstart]" class="js-boxberry-parcel-points-list">
                <?php  $_smarty_tpl->tpl_vars['point_data'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['point_data']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['points_by_settings']->value['points']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['point_data']->key => $_smarty_tpl->tpl_vars['point_data']->value){
$_smarty_tpl->tpl_vars['point_data']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['point_data']->value['code'];?>
" <?php if ($_smarty_tpl->tpl_vars['points_by_settings']->value['targetstart']==$_smarty_tpl->tpl_vars['point_data']->value['code']){?>selected<?php }?>>
                       <?php echo $_smarty_tpl->tpl_vars['point_data']->value['name'];?>

                    </option>
                <?php } ?>
            </select>
        </div>
    </div>

    
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Default weight');?>
</div>
        <div class="value">
            <input max="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_weight'])===null||$tmp==='' ? '30000' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" min="5" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[default_weight]" step="any" type="number"
                   value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['default_weight'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('g');?>

        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Default length');?>
</div>
        <div class="value">
            <input max="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_length'])===null||$tmp==='' ? '1.2' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" min="0.0001" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[default_length]" required step="0.0001" type="number"
                   value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['default_length'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('m');?>

        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Default width');?>
</div>
        <div class="value">
            <input max="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_width'])===null||$tmp==='' ? '0.8' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" min="0.0001" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[default_width]" required step="0.0001" type="number"
                   value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['default_width'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('m');?>

        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Default height');?>
</div>
        <div class="value">
            <input max="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_height'])===null||$tmp==='' ? '0.5' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" min="0.0001" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[default_height]" required step="0.0001" type="number"
                   value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['default_height'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('m');?>

            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Used only if order dimensions have not been calculated by a special plugin. If no value is specified, shipping rate and terms will not be calulated.');?>
</p>
        </div>
    </div>
    
    <div class="field">
        <div class="value">
            <p class="hint bold"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('The sum of length, width, and height values must not exceed 2.5 m.');?>
</p>
        </div>
    </div>
</div>
<?php }} ?><?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:16
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/include.settings_available_shipping.html" */ ?>
<?php if ($_valid && !is_callable('content_5eb29fc858c847_64172475')) {function content_5eb29fc858c847_64172475($_smarty_tpl) {?><div class="field-group">
    <div class="field">
        <div class="name"></div>
        <div class="value"><h3><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Availability settings');?>
</h3></div>
    </div>

    
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Country');?>
</div>
        <div class="value no-shift">
            <span>Российская Федерация</span>
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[country]" value="rus">
        </div>
    </div>
    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Region');?>

        </div>
        <div class="value">
            <select name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[region]">
                <option value=""></option>
                <?php  $_smarty_tpl->tpl_vars['region'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['region']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['regions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['region']->key => $_smarty_tpl->tpl_vars['region']->value){
$_smarty_tpl->tpl_vars['region']->_loop = true;
?>
                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['region']->value['code'], ENT_QUOTES, 'UTF-8', true);?>
"<?php if (htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['region'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true)==$_smarty_tpl->tpl_vars['region']->value['code']){?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['region']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                <?php } ?>
            </select>
            <p class="small"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Shipping will be restricted to the selected region.');?>
</p>
        </div>
    </div>
    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Localities');?>

        </div>
        <div class="value">
            <input name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[cities]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['cities'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="text">
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Separate locality names with a comma.');?>
</p>
        </div>
    </div>

    
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Max weight');?>
</div>
        <div class="value">
            <input name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[max_weight]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_weight'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="5" max="30000" step="any">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('g');?>

        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Max length');?>
</div>
        <div class="value">
            <input name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[max_length]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_length'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="0.001" max="1.2">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('m');?>

        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Max width');?>
</div>
        <div class="value">
            <input name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[max_width]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_width'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="0.0001" max="0.8">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('m');?>

        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Max height');?>
</div>
        <div class="value">
            <input name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[max_height]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_height'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="0.0001" max="0.5">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('m');?>

        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Max dimensions');?>
</div>
        <div class="value">
            <input name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[max_dimensions]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_dimensions'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="0.0001" max="2.5">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('m');?>

        </div>
    </div>
    
    <div class="field">
        <div class="value">
            <p class="hint bold"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('The sum of length, width, and height values must not exceed 2.5 m.');?>
</p>
        </div>
    </div>
</div>
<?php }} ?><?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:16
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/include.settings_shipping_cost.html" */ ?>
<?php if ($_valid && !is_callable('content_5eb29fc85b42c0_94306797')) {function content_5eb29fc85b42c0_94306797($_smarty_tpl) {?><div class="field-group">
    
    <div class="field">
        <div class="name"></div>
        <div class="value"><h3><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Shipping cost');?>
</h3></div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Minimum order cost for free delivery');?>
</div>
        <div class="value">
            <input name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[free_price]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['free_price'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
            <span>RUB</span>
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Shipping rates provided by Boxberry service are ignored when free shipping is applied.');?>
</p>
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Declared value for prepaid delivery (insurance cost)');?>
</div>
        <div class="value">
            <input name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[declared_price]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['declared_price'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="text">
            <p class="hint" style="white-space: pre-line">
                <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Specify either a fixed value expressed in a currency or percentage of the order total, or their sum or difference.');?>

                <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Examples:');?>

                0
                123.45
                12.23%
                123.45+12.23%
                123.45-12.23%

                <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Declared value must not exceed 300,000 rubles.');?>

            </p>
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('In case of payment on delivery, the declared value passed to the shipping service is equal to the order total less the shipping cost, i.e. pure cost of delivered items with discount applied.');?>
</p>
            <p class="hint"><em><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Set up markup costs in your Boxberry account.');?>
</em></p>
        </div>
    </div>
</div><?php }} ?><?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:16
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/include.settings_shipping_options.html" */ ?>
<?php if ($_valid && !is_callable('content_5eb29fc85c45c2_64771581')) {function content_5eb29fc85c45c2_64771581($_smarty_tpl) {?><div class="field-group">
    
    <div class="field">
        <div class="name"></div>
        <div class="value"><h3><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Shipping options');?>
</h3></div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Pickup points');?>
</div>
        <div class="value">
            <select name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[point_mode]">
                <?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['point_modes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value){
$_smarty_tpl->tpl_vars['option']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['option']->value['value'];?>
" <?php if ($_smarty_tpl->tpl_vars['option']->value['value']===$_smarty_tpl->tpl_vars['settings']->value['point_mode']){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['option']->value['title'];?>
</option>
                <?php } ?>
            </select>
        </div>
        <div class="value">
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Select values corresponding to your contract’s conditions.');?>

                <br>
                <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('If “With prepayment only” option is selected, then in Shop-Script, with “In-cart checkout” mode enabled, only prepayment options are available after shipping option selection.');?>

                <br>
                <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('If “All” option is selected, then in Shop-Script, with “In-cart checkout” mode enabled, minimum shipping cost for prepayment is displayed by default. Once a shipping and a payment option are selected, the displayed shipping cost is updated accordingly.');?>
</p>
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Courier delivery');?>
</div>
        <div class="value">
            <select name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[courier_mode]">
                <?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['courier_modes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value){
$_smarty_tpl->tpl_vars['option']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['option']->value['value'];?>
" <?php if ($_smarty_tpl->tpl_vars['option']->value['value']===$_smarty_tpl->tpl_vars['settings']->value['courier_mode']){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['option']->value['title'];?>
</option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Courier name');?>
</div>
        <div class="value">
            <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[courier_title]" value="<?php echo ifset($_smarty_tpl->tpl_vars['settings']->value,'courier_title','');?>
">
        </div>
    </div>
</div><?php }} ?><?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:16
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/boxberry/templates/include.settings_integration.html" */ ?>
<?php if ($_valid && !is_callable('content_5eb29fc85dae61_10223421')) {function content_5eb29fc85dae61_10223421($_smarty_tpl) {?><div class="field-group">
    
    <div class="field">
        <div class="name"></div>
        <div class="value"><h3><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Integration settings');?>
</h3></div>
        <div class="value"><p class="hint"><em><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Upon checkout, a dellivery draft is created in your Boxberry account. Should you edit an order in your Webasyst backend, the corresponding draft is updated automatically.');?>
</em></p></div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Type of parcel delivery');?>
</div>
        <div class="value">
            <select name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[issuance]">
                <?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['issuance_options']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value){
$_smarty_tpl->tpl_vars['option']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['option']->value['value'];?>
" <?php if ($_smarty_tpl->tpl_vars['option']->value['value']===$_smarty_tpl->tpl_vars['settings']->value['issuance']){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['option']->value['title'];?>
</option>
                <?php } ?>
            </select>
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('If nothing is selected, a default value from the store profile’s “Services” section of your Boxberry account is used.');?>
</p>
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Store name for SMS and email notifications');?>
</div>
        <div class="value">
            <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[notification_name]" value="<?php echo $_smarty_tpl->tpl_vars['settings']->value['notification_name'];?>
">
            <br>
            <span class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Notifications are sent by Boxberry service.');?>
</span>
        </div>
    </div>
</div><?php }} ?>