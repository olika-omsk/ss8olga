<?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:29
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/courier/templates/settings.html" */ ?>
<?php /*%%SmartyHeaderCode:8756447005eb29fd5cea630-63858328%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '12c3ca86f585067664f18b8c540511e386476f79' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/courier/templates/settings.html',
      1 => 1581578120,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8756447005eb29fd5cea630-63858328',
  'function' => 
  array (
    '_shipping_courier_additional_subfield_select_options' => 
    array (
      'parameter' => 
      array (
        'selected' => '',
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'namespace' => 0,
    'p' => 0,
    'values' => 0,
    'countries' => 0,
    'country' => 0,
    'regions' => 0,
    'region' => 0,
    'address_oneline_string_subfields' => 0,
    '_subfield_id' => 0,
    'selected' => 0,
    '_subfield' => 0,
    'additional_address_field_ids' => 0,
    '_field_id' => 0,
    '_real_subfield_id' => 0,
    '_string_subfield_count' => 0,
    '_checkbox_is_disabled' => 0,
    '_checkbox_is_selected' => 0,
    '_label' => 0,
    'webasyst_app_url' => 0,
    '_link_url' => 0,
    'map_adapters' => 0,
    'a' => 0,
    'adapter_id' => 0,
    'map_adapter_settings' => 0,
    'currencies' => 0,
    'currency' => 0,
    'rate' => 0,
    'js_code' => 0,
    '_locale_strings' => 0,
    'xhr_url' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5eb29fd5d99c21_23442823',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5eb29fd5d99c21_23442823')) {function content_5eb29fd5d99c21_23442823($_smarty_tpl) {?><div id="courier-pickup-settings">
    <div class="field-group">
        <div class="field">
            <div class="name"><label for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[rate_zone][country]"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Shipping region');?>
</label></div>
            <div class="value">
                <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[rate_zone][country]" title="<?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Shipping region');?>
">
                    <option value=""<?php if (empty($_smarty_tpl->tpl_vars['values']->value['rate_zone']['country'])){?> selected="selected"<?php }?>></option>
                    <?php  $_smarty_tpl->tpl_vars['country'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['country']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['country']->key => $_smarty_tpl->tpl_vars['country']->value){
$_smarty_tpl->tpl_vars['country']->_loop = true;
?>
                        <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value['iso3letter'], ENT_QUOTES, 'UTF-8', true);?>
"<?php if (!empty($_smarty_tpl->tpl_vars['values']->value['rate_zone']['country'])&&($_smarty_tpl->tpl_vars['values']->value['rate_zone']['country']==$_smarty_tpl->tpl_vars['country']->value['iso3letter'])){?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="field">
            <div class="value region">
                <i class="icon16 loading" style="display:none; margin-left: -23px;"></i>
                <?php if (empty($_smarty_tpl->tpl_vars['regions']->value)){?>
                    <p class="small">
                        Shipping will be restricted to the selected country. Rates defined below will be applied to the entire country.
                    </p>
                    <input name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[rate_zone][region]" value="" type="hidden">
                <?php }else{ ?>
                    <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[rate_zone][region]" title="">
                        <option value=""<?php if (empty($_smarty_tpl->tpl_vars['values']->value['rate_zone']['region'])){?> selected="selected"<?php }?>></option>
                        <?php  $_smarty_tpl->tpl_vars['region'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['region']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['regions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['region']->key => $_smarty_tpl->tpl_vars['region']->value){
$_smarty_tpl->tpl_vars['region']->_loop = true;
?>
                            <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['region']->value['code'], ENT_QUOTES, 'UTF-8', true);?>
"<?php if (ifset($_smarty_tpl->tpl_vars['values']->value['rate_zone']['region'])==$_smarty_tpl->tpl_vars['region']->value['code']){?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['region']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                        <?php } ?>
                    </select>
                    <br>
                    <br>
                    <p class="small"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Shipping will be restricted to the selected region.');?>
</p>
                <?php }?>
            </div>
        </div>

        <div class="field">
            <div class="name">
    
            </div>
            <div class="value">
                <label><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Locality name');?>
: <input name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[rate_zone][city]" value="<?php echo ifset($_smarty_tpl->tpl_vars['values']->value['rate_zone']['city']);?>
" type="text"></label>
    
                <p class="hint"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('If courier shipping is provided only within specific localities, enter the locality names separated by comma so that customer is not be prompted to enter a locality name when this shipping option is selected; a locality name will be automatically fetched as you specify it here. If this field is left blank, a customer will be prompted to enter a locality name.');?>
</p>
            </div>
            <br><br>
        </div>

        <div class="field">
            <div class="name"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Address fields management');?>
</div>
            <div class="value">
                <input name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[required_fields]" type="hidden" value="">
                <label><input name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[required_fields]"  type="checkbox" value="1" <?php if (!empty($_smarty_tpl->tpl_vars['values']->value['required_fields'])){?> checked="checked"<?php }?>><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Use plugin‘s default fields only');?>
</label>
                <p class="hint"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Enable this option for the plugin to require from a client <em>mandatory</em> completion of the fields only, which are selected below in setting “Default address fields”.<br>Disable it to make the plugin <em>optionally</em> request only address fields defined by app settings (the app must request all address fields required for selected shipping method).');?>
</p>
                <br>
            </div>
        </div>
    
        <div class="field">
            <div class="name"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Default address fields');?>
</div>
            <div class="value"><label><input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[contact_fields][country]" value="country" <?php if (!empty($_smarty_tpl->tpl_vars['values']->value['contact_fields']['country'])||!empty($_smarty_tpl->tpl_vars['values']->value['rate_zone']['country'])){?> checked="checked"<?php }?> <?php if (!empty($_smarty_tpl->tpl_vars['values']->value['rate_zone']['country'])){?> disabled="disabled"<?php }?>>&nbsp;Страна</label></div>
            <div class="value"><label><input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[contact_fields][region]" value="region" <?php if (!empty($_smarty_tpl->tpl_vars['values']->value['contact_fields']['region'])||!empty($_smarty_tpl->tpl_vars['values']->value['rate_zone']['region'])){?> checked="checked"<?php }?> <?php if (!empty($_smarty_tpl->tpl_vars['values']->value['rate_zone']['region'])){?> disabled="disabled"<?php }?>>&nbsp;Регион</label></div>
            <div class="value"><label><input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[contact_fields][city]" value="city" <?php if (!empty($_smarty_tpl->tpl_vars['values']->value['contact_fields']['city'])||!empty($_smarty_tpl->tpl_vars['values']->value['rate_zone']['city'])){?> checked="checked"<?php }?><?php if (!empty($_smarty_tpl->tpl_vars['values']->value['rate_zone']['city'])){?> disabled="disabled"<?php }?>>&nbsp;Город</label></div>
            <div class="value"><label><input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[contact_fields][street]" value="street" <?php if (!empty($_smarty_tpl->tpl_vars['values']->value['contact_fields']['street'])){?> checked="checked"<?php }?>>&nbsp;Улица, дом, квартира</label></div>
            <div class="value"><label><input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[contact_fields][zip]" value="zip" <?php if (!empty($_smarty_tpl->tpl_vars['values']->value['contact_fields']['zip'])){?>checked="checked"<?php }?>>&nbsp;Индекс</label></div>
            <div class="value hint"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Enable shipping address fields which must always be filled by a client during checkout.');?>
<br>
	            <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Should some of the enabled fields be filled on the contact info step, then they will not be displayed on courier shipping selection.');?>
<br>
                <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Disabled can be only fields that are not completed in the above setting “Shipping region”.');?>
</div>
            <br><br>
        </div>


        <div class="field js-additional-address-fields">
            <div class="name"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Additional address fields');?>
</div>

            <?php $_smarty_tpl->tpl_vars['_string_subfield_count'] = new Smarty_variable(count($_smarty_tpl->tpl_vars['address_oneline_string_subfields']->value), null, 0);?>

            <?php if (!function_exists('smarty_template_function__shipping_courier_additional_subfield_select_options')) {
    function smarty_template_function__shipping_courier_additional_subfield_select_options($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['_shipping_courier_additional_subfield_select_options']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
                <?php  $_smarty_tpl->tpl_vars['_subfield'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['_subfield']->_loop = false;
 $_smarty_tpl->tpl_vars['_subfield_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['address_oneline_string_subfields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['_subfield']->key => $_smarty_tpl->tpl_vars['_subfield']->value){
$_smarty_tpl->tpl_vars['_subfield']->_loop = true;
 $_smarty_tpl->tpl_vars['_subfield_id']->value = $_smarty_tpl->tpl_vars['_subfield']->key;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['_subfield_id']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['selected']->value==$_smarty_tpl->tpl_vars['_subfield_id']->value){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['_subfield']->value->getName();?>
</option>
                <?php } ?>
            <?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>



            <?php  $_smarty_tpl->tpl_vars['_label'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['_label']->_loop = false;
 $_smarty_tpl->tpl_vars['_field_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['additional_address_field_ids']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['_label']->key => $_smarty_tpl->tpl_vars['_label']->value){
$_smarty_tpl->tpl_vars['_label']->_loop = true;
 $_smarty_tpl->tpl_vars['_field_id']->value = $_smarty_tpl->tpl_vars['_label']->key;
?>

                <?php $_smarty_tpl->tpl_vars['_real_subfield_id'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['values']->value['additional_address_fields'][$_smarty_tpl->tpl_vars['_field_id']->value])===null||$tmp==='' ? '' : $tmp), null, 0);?>
                <?php if (!isset($_smarty_tpl->tpl_vars['address_oneline_string_subfields']->value[$_smarty_tpl->tpl_vars['_real_subfield_id']->value])){?>
                    <?php $_smarty_tpl->tpl_vars['_real_subfield_id'] = new Smarty_variable('', null, 0);?>
                <?php }?>

                <?php $_smarty_tpl->tpl_vars['_checkbox_is_disabled'] = new Smarty_variable($_smarty_tpl->tpl_vars['_string_subfield_count']->value==0, null, 0);?>
                <?php $_smarty_tpl->tpl_vars['_checkbox_is_selected'] = new Smarty_variable(!$_smarty_tpl->tpl_vars['_checkbox_is_disabled']->value&&$_smarty_tpl->tpl_vars['_real_subfield_id']->value, null, 0);?>

                <div class="value">
                    <label>
                        <input type="checkbox" data-field-id="<?php echo $_smarty_tpl->tpl_vars['_field_id']->value;?>
"
                            <?php if ($_smarty_tpl->tpl_vars['_checkbox_is_disabled']->value){?>disabled="disabled"<?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['_checkbox_is_selected']->value){?>checked="checked"<?php }?>>&nbsp;<?php echo $_smarty_tpl->tpl_vars['_label']->value;?>

                    </label>
                </div>
                <div class="value js-select-wrapper" <?php if (!$_smarty_tpl->tpl_vars['_checkbox_is_selected']->value){?>style="display: none"<?php }?>>
                    
                    <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[additional_address_fields][<?php echo $_smarty_tpl->tpl_vars['_field_id']->value;?>
]" value="">
                    <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[additional_address_fields][<?php echo $_smarty_tpl->tpl_vars['_field_id']->value;?>
]" data-field-id="<?php echo $_smarty_tpl->tpl_vars['_field_id']->value;?>
" <?php if (!$_smarty_tpl->tpl_vars['_checkbox_is_selected']->value){?>disabled="disabled"<?php }?>>
                        <?php smarty_template_function__shipping_courier_additional_subfield_select_options($_smarty_tpl,array('selected'=>$_smarty_tpl->tpl_vars['_real_subfield_id']->value));?>

                    </select>
                </div>
            <?php } ?>

            <div class="value hint">
                <?php if ($_smarty_tpl->tpl_vars['_string_subfield_count']->value==0){?>
                    <div>
                        <?php $_smarty_tpl->tpl_vars['_link_url'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['webasyst_app_url']->value)."settings/field/", null, 0);?>
                        <i class="icon16 exclamation"></i>
                        <?php echo sprintf($_smarty_tpl->tpl_vars['p']->value->_w('Add extra subfields of “<em>Text (input)</em>” type for the “Address” field via <a href="%s">Settings</a> app to enable these additional address fields.'),$_smarty_tpl->tpl_vars['_link_url']->value);?>

                    </div>
                <?php }?>
                <div><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Enable extra address fields which you want to be offered for completion to customers during checkout.');?>
</div>
                <div class="js-additional-address-fields-errors errormsg" style="display: none"></div>
            </div>

            <br><br>
        </div>

        <?php if (!empty($_smarty_tpl->tpl_vars['map_adapters']->value)){?>
            <div class="field">
                <div class="name">Карты</div>
                <?php  $_smarty_tpl->tpl_vars['a'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['a']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['map_adapters']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['a']->key => $_smarty_tpl->tpl_vars['a']->value){
$_smarty_tpl->tpl_vars['a']->_loop = true;
?>
                    <?php $_smarty_tpl->tpl_vars['adapter_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['a']->value->getId(), null, 0);?>
                    <div class="value no-shift">
                        <label>
                            <input type="radio" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[map]" <?php if ($_smarty_tpl->tpl_vars['values']->value['map']==$_smarty_tpl->tpl_vars['adapter_id']->value){?>checked<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['adapter_id']->value;?>
">&nbsp;<?php echo $_smarty_tpl->tpl_vars['a']->value->getName();?>

                        </label>

                    </div>
                    <?php $_smarty_tpl->tpl_vars['map_adapter_settings'] = new Smarty_variable($_smarty_tpl->tpl_vars['a']->value->getSettingsHtml(array('namespace'=>$_smarty_tpl->tpl_vars['namespace']->value)), null, 0);?>
                    <?php if (!empty($_smarty_tpl->tpl_vars['map_adapter_settings']->value)){?>
                        <div class="value no-shift hint js-map-adapter-settings" data-adapter-id="<?php echo $_smarty_tpl->tpl_vars['adapter_id']->value;?>
" style="display: none;">
                            <?php echo sprintf($_smarty_tpl->tpl_vars['p']->value->_w('Online map settings are available in <a href="%s">Settings</a> app.'),'../webasyst/settings/maps/');?>

                        </div>
                    <?php }?>
                <?php } ?>
            </div>
        <?php }?>
    </div>

    <div class="field-group">
        <div class="field">
            <div class="name">
                <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Shipping rate calculation');?>

            </div>
            <div class="value">
                <label>
                    <input name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[rate_by]"
                           value="weight"<?php if ($_smarty_tpl->tpl_vars['values']->value['rate_by']=='weight'){?> checked="checked"<?php }?>
                           type="radio"> <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('By weight');?>

                </label>
            </div>
            <div class="value">
                <label>
                    <input name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[rate_by]"
                           value="price"<?php if ($_smarty_tpl->tpl_vars['values']->value['rate_by']=='price'){?> checked="checked" <?php }?>
                           type="radio"> <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('By price');?>

                </label>
            </div>
        </div>
    
    
        <div class="field">
            <div class="value">
                <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[currency]" title="">
                    <?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value){
$_smarty_tpl->tpl_vars['currency']->_loop = true;
?>
                        <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['code'], ENT_QUOTES, 'UTF-8', true);?>
"
                                <?php if ($_smarty_tpl->tpl_vars['values']->value['currency']==$_smarty_tpl->tpl_vars['currency']->value['code']){?> selected="selected"<?php }?>>
                            <?php echo htmlspecialchars(((string)$_smarty_tpl->tpl_vars['currency']->value['title'])." (".((string)$_smarty_tpl->tpl_vars['currency']->value['code']).")", ENT_QUOTES, 'UTF-8', true);?>

                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="field">
            <div class="value">
                <table class="zebra">
                    <thead>
                    <tr class="white">
                        <th colspan="2" class="rate-by">
                                <span class="weight" <?php if ($_smarty_tpl->tpl_vars['values']->value['rate_by']!='weight'){?>style="display:none;"<?php }?>>
                                    <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Shipment weight');?>

                                </span>
                            <span class="price" <?php if ($_smarty_tpl->tpl_vars['values']->value['rate_by']!='price'){?>style="display:none;"<?php }?>>
                                    <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Shipment amount');?>

                                </span>
                        </th>
                        <th>&nbsp;</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Shipping rate');?>
</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr class="white">
                        <td colspan="3">
                            <a class="add-rate inline-link" href="#">
                                <i class="icon16 add"></i> <b><i><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Add rate');?>
</i></b>
                            </a>
                        </td>
                        <td>
                            <span class="hint"><?php echo sprintf($_smarty_tpl->tpl_vars['p']->value->_w('Enter shipping rate as a flat rate, as a percent of cart total, or as a sum of both. Example: <b>20+10%%</b> will calculate shipping rate as 20 %s + 10%% of cart total.'),$_smarty_tpl->tpl_vars['values']->value['currency']);?>
</span>
                        </td>
                        <td></td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php  $_smarty_tpl->tpl_vars['rate'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['rate']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['values']->value['rate']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['rate']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['rate']->key => $_smarty_tpl->tpl_vars['rate']->value){
$_smarty_tpl->tpl_vars['rate']->_loop = true;
 $_smarty_tpl->tpl_vars['rate']->index++;
?>
                        <tr class="rate">
                            <td class="min-width">&gt;</td>
                            <td title="<?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Shipment amount');?>
">
                                <input type="text"
                                       value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rate']->value['limit'], ENT_QUOTES, 'UTF-8', true);?>
"
                                       class="refreshable input short numerical"
                                       name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[rate][<?php echo $_smarty_tpl->tpl_vars['rate']->index;?>
][limit]"
                                >
                                <span class="rate-by">
                                    <span class="weight dimension" <?php if ($_smarty_tpl->tpl_vars['values']->value['rate_by']!='weight'){?>style="display:none;"<?php }?>>
                                        <?php if ($_smarty_tpl->tpl_vars['values']->value['weight_dimension']=='kg'){?>
                                            <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('kg');?>

                                        <?php }else{ ?>
                                            <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('lbs');?>

                                        <?php }?>
                                    </span>
                                    <span class="price currency" <?php if ($_smarty_tpl->tpl_vars['values']->value['rate_by']!='price'){?>style="display:none;"<?php }?>>
                                        <?php echo $_smarty_tpl->tpl_vars['values']->value['currency'];?>

                                    </span>
                                </span>
                            </td>
                            <td>→</td>
                            <td title="<?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Shipping rate');?>
">
                                <input type="text"
                                       value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rate']->value['cost'], ENT_QUOTES, 'UTF-8', true);?>
"
                                       class="short numerical"
                                       name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[rate][<?php echo $_smarty_tpl->tpl_vars['rate']->index;?>
][cost]"
                                >
                                <span class="currency"><?php echo $_smarty_tpl->tpl_vars['values']->value['currency'];?>
</span>
                            </td>
                            <td>
                                <a class="delete-rate" href="#">
                                    <i class="icon16 delete"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <br><br>
            </div>
        </div>
    
        <div class="field">
            <div class="name">
                <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Weight dimension unit');?>

            </div>
            <div class="value">
                <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[weight_dimension]">
                    <option value="kg" data-value="<?php echo $_smarty_tpl->tpl_vars['p']->value->_w('kg');?>
"
                            <?php if ($_smarty_tpl->tpl_vars['values']->value['weight_dimension']=='kg'){?> selected="selected"<?php }?>>
                        <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('kg');?>

                    </option>
                    <option value="lbs" data-value="<?php echo $_smarty_tpl->tpl_vars['p']->value->_w('lbs');?>
"
                            <?php if ($_smarty_tpl->tpl_vars['values']->value['weight_dimension']=='lbs'){?> selected="selected"<?php }?>>
                        <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('lbs');?>

                    </option>
                </select>
            </div>
        </div>
    </div>
</div>

<?php echo $_smarty_tpl->tpl_vars['js_code']->value;?>


<script type="text/javascript">

    <?php $_smarty_tpl->tpl_vars['_locale_strings'] = new Smarty_variable(array('additional_address_field_assign_error'=>$_smarty_tpl->tpl_vars['p']->value->_w('Please select different address fields for each additional field.')), null, 0);?>

    new waCourierShippingPluginSettings({
        $wrapper: $('#courier-pickup-settings'),
        locale_strings: <?php echo json_encode($_smarty_tpl->tpl_vars['_locale_strings']->value);?>
,
        namespace: <?php echo json_encode(htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true));?>
,
        xhr_url: <?php echo json_encode(htmlspecialchars($_smarty_tpl->tpl_vars['xhr_url']->value, ENT_QUOTES, 'UTF-8', true));?>
,
        js_validate: true
    });

</script>
<?php }} ?>