<?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:44
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/worldwide/templates/settings.html" */ ?>
<?php /*%%SmartyHeaderCode:875660175eb29fe4561c19-99915130%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '210021836109b32e55a864b95b5824e49149a966' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/worldwide/templates/settings.html',
      1 => 1581578120,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '875660175eb29fe4561c19-99915130',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'plid' => 0,
    'services_by_type' => 0,
    'p' => 0,
    'namespace' => 0,
    'values' => 0,
    'currencies' => 0,
    'currency' => 0,
    'countries' => 0,
    'c' => 0,
    'delivery_countries' => 0,
    'country_names' => 0,
    'regions' => 0,
    'transits' => 0,
    'wa_url' => 0,
    'xhr_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5eb29fe45e9434_18225717',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5eb29fe45e9434_18225717')) {function content_5eb29fe45e9434_18225717($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/var/www/novikova_ss8olga/wa-system/vendors/smarty3/plugins/modifier.escape.php';
?><style>
    #worldwide-delivery-table { margin-top: -10px; }
    #<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table tr.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-disabled td { text-decoration: line-through; color: #aaa; }
    .<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-disable-region-link i.delete { opacity: 0; }
    .<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-disable-region-link:hover i.delete { opacity: 1; }
</style>
<div class="field-group" id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-deliverty-settings">
    <?php if ($_smarty_tpl->tpl_vars['services_by_type']->value){?>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Shipping service name');?>
</div>
        <div class="value"><input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[service_name]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['values']->value['service_name'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
"></div>
        <div class="value hint"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Enter optional shipping service name which will be shown to customers next to this shipping option');?>
</div>
    </div>
    <?php }?>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Currency');?>
</div>
        <div class="value">
            <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[currency]" id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-currency-input">
                <?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value){
$_smarty_tpl->tpl_vars['currency']->_loop = true;
?>
                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['code'], ENT_QUOTES, 'UTF-8', true);?>
"
                            <?php if ($_smarty_tpl->tpl_vars['values']->value['currency']==$_smarty_tpl->tpl_vars['currency']->value['code']){?> selected="selected"<?php }?>>
                        <?php echo htmlspecialchars(((string)$_smarty_tpl->tpl_vars['currency']->value['title'])." (".((string)$_smarty_tpl->tpl_vars['currency']->value['code']).")", ENT_QUOTES, 'UTF-8', true);?>

                    </option>
                <?php } ?>
            </select>
        </div>
    </div>


    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Weight dimension unit');?>

        </div>
        <div class="value">
            <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[weight_dimension]" id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-weight-unit-input">
                <option value="kg" data-value="<?php echo $_smarty_tpl->tpl_vars['p']->value->_w('kg');?>
"
                        <?php if ($_smarty_tpl->tpl_vars['values']->value['weight_dimension']=='kg'){?> selected="selected"<?php }?>>
                    <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('kg');?>

                </option>
                <option value="lbs" data-value="<?php echo $_smarty_tpl->tpl_vars['p']->value->_w('lbs');?>
"
                        <?php if ($_smarty_tpl->tpl_vars['values']->value['weight_dimension']=='lbs'){?> selected="selected"<?php }?>>
                    <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('lbs');?>

                </option>
            </select>
        </div>
    </div>

    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Store country');?>

        </div>
        <div class="value">
            <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[own_country]" id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-own-country">
                <option selected value=""></option>
                <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value){
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['c']->value['iso3letter'];?>
" <?php if (!empty($_smarty_tpl->tpl_vars['values']->value['own_country'])&&$_smarty_tpl->tpl_vars['values']->value['own_country']==$_smarty_tpl->tpl_vars['c']->value['iso3letter']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
</option>
                <?php } ?>
            </select>
            <br>
            <span class="hint"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Select a country within which worldwide shipping should not be available.');?>
</span>
        </div>
    </div>

    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Delivery rates');?>

        </div>
        <div class="value no-shift">

            <table id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table" class="zebra">
                <thead  id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table-head">
                    <tr class="white">
                        <th colspan="2" class="nowrap <?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-country-column"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Destination');?>
 <i class="icon10 uarr"></i></th>
                        <th id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-calculation-type-th">
                            <select id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-calculation-type">
                                <option value="all"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Same rate for any package weight');?>
</option>
                                <option value="individually"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Define rates by package weight...');?>
</option>
                            </select>
                        </th>
                        <th><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Transit time');?>
</th>
                        <th class="min-width"></th>
                    </tr>
                </thead>
                <tbody id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table-items">
                </tbody>
                <tbody>

                    <tr class="empty-row<?php if ($_smarty_tpl->tpl_vars['delivery_countries']->value){?> hidden<?php }?>">
                        <td colspan="5">
                            <em class="grey"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('No international shipping rates defined yet.');?>
</em>
                        </td>
                    </tr>

                    <tr class="white">
                        <td colspan="5"></td>
                    </tr>

                    <tr class="white">
                        <td colspan="5">
                            <select id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-add-new-country">
                                <option selected value=""><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Add country');?>
</option>
                                <option disabled>--</option>
                                <optgroup label="">
                                <option value="%AL"<?php if (!empty($_smarty_tpl->tpl_vars['delivery_countries']->value)){?> disabled<?php }?>><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('All countries');?>
</option>
                                <option value="%EU"<?php if (!empty($_smarty_tpl->tpl_vars['delivery_countries']->value["%EU"])){?> disabled<?php }?>><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('All European countries');?>
</option>
                                <option value="%RW"<?php if (!empty($_smarty_tpl->tpl_vars['delivery_countries']->value["%RW"])){?> disabled<?php }?>><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Rest of world');?>
</option>
                                <option disabled>--</option>
                                <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value){
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['c']->value['iso3letter'];?>
"<?php if (!empty($_smarty_tpl->tpl_vars['delivery_countries']->value[$_smarty_tpl->tpl_vars['c']->value['iso3letter']])||!$_smarty_tpl->tpl_vars['c']->value['iso3letter']){?> disabled<?php }?>><?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
</option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>

            <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[delivery_table]" id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table-hidden-input">
            <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['namespace']->value, ENT_QUOTES, 'UTF-8', true);?>
[weights]" id="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-weights-hidden-input">

        </div>

    </div>
</div>


<script>(function() {

    var countries = <?php echo json_encode($_smarty_tpl->tpl_vars['country_names']->value);?>
;
    countries['%AL'] = '<?php echo $_smarty_tpl->tpl_vars['p']->value->_w('All countries');?>
';
    countries['%EU'] = '<?php echo $_smarty_tpl->tpl_vars['p']->value->_w('All European countries');?>
';
    countries['%RW'] = '<?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Rest of world');?>
';
    var regions = <?php echo json_encode($_smarty_tpl->tpl_vars['regions']->value);?>
;

    var items = <?php echo json_encode($_smarty_tpl->tpl_vars['values']->value['delivery_table']);?>
;
    var weights = <?php echo json_encode($_smarty_tpl->tpl_vars['values']->value['weights']);?>
;
    var transits = <?php echo json_encode($_smarty_tpl->tpl_vars['transits']->value);?>
;

    var findByCountryAndRegion = function(items, country, region) {
        for (var i = 0; i < items.length; i ++) {
            if (items[i].country === country) {
                if (!region) {
                    return items[i];
                } else if (items[i].items) {
                    for (var j = 0; j < items[i].items.length; j ++) {
                        if (items[i].items[j].region == region) {
                            return items[i].items[j];
                        }
                    }
                }
                break;
            }
        }
        return null;
    };

    var deleteByCountry = function(items, country) {
        for (var i = 0; i < items.length; i += 1) {
            if (items[i].country === country) {
                items = items.slice(0, i).concat(items.slice(i + 1));
                return items;
            }
        }
        return items;
    };

        var toFloat = function(val) {
            var str = ('' + (val || '0')).replace(',', '.').replace(/["']*/, '');
            return parseFloat(str) || 0.0;
        };

        var toRate = function(val) {
            return ('' + (val || '')).replace(',', '.').replace(/[^\d\.\-+%]+/, '');
        };

    var formatValues = function() {
        for (var i = 0; i < weights.length; i += 1) {
            if (weights[i]) {
                weights[i] = toFloat(weights[i]);
            }
        }
        var toFloatItemsRates = function (items) {
            for (var i = 0; i < items.length; i += 1) {
                var item = items[i];
                for (var j = 0; j < item.rate.length; j += 1) {
                    item.rate[j] = toRate(item.rate[j] || '');
                }
                if (!$.isEmptyObject(item.items)) {
                    toFloatItemsRates(item.items);
                }
            }
        };
        toFloatItemsRates(items);
    };

    var upHiddenInputs = function() {
        $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table-hidden-input').val(JSON.stringify(items));
        $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-weights-hidden-input').val(JSON.stringify(weights));
    };

    var renderDeliveryTableHead = function(weights) {
        var weight_unit = $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-weight-unit-input').val();
        var th = '';
        th += '<th colspan="2" class="nowrap <?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-country-column"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Destination');?>
 <i class="icon10 uarr"></i></th>';
        if (!$.isArray(weights)||weights.length==0) {
            th += '<th>' + renderCalculationTypeSelector() + '</th>';
        } else {
            for (var i = 0; i < weights.length; i += 1) {
                th += '<th class="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-weight-td" data-index="' + i + '"><span style="margin-left: -0.85em;">≥</span>&nbsp;<input type="text" class="short bold numerical <?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-weight-input" placeholder="0" value="' + weights[i] + '">&nbsp;<span class="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-weight-unit">'+weight_unit+'</span>&nbsp;';
                th += '<a href="javascript:void(0);" class="inline"><i class="icon10 delete <?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delete-weight-link" style="margin-top: 6px;"></i></a></th>';
            }
            th += '<th><a href="javascript:void(0)" class="inline <?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-add-column-link inline-link small"><i class="icon10 add"></i><b><i><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Add weight');?>
</i></b></a></th>';
        }
        th += '<th><?php echo $_smarty_tpl->tpl_vars['p']->value->_w("Transit time");?>
</th>';
        th += '<th></th>';
        return '<tr class="white">' + th + '</th>';
    };

    var renderDeliveryTableBodyRows = function(items, weights, currency) {
        if(!currency){
            currency = $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-currency-input').val();
        }
        var res = '';
        for (var it = 0; it < items.length; it += 1) {
            var item = items[it];
            var country = item.country || '';
            var region = item.region || '';
            var name = '';
            if (region && regions[country]) {
                name = regions[country].options[region];
            } else if (!region) {
                name = countries[country];
            }
            var icon = '';
            if (country && country.slice(0, 1) !== '%' && !region) {
                icon = '<i class="icon16 country" style="background:url(<?php echo $_smarty_tpl->tpl_vars['wa_url']->value;?>
wa-content/img/country/' + country + '.gif) no-repeat"></i>';
            }

            var name_html = '';
            if (region) {
                name_html = '<td class="small"><span>' + name + '</span>' + ' <span class="hint">' + region + '</span>' + '</td>';
            } else {
                name_html = '<td><span>' + name + '</span></td>';
            }

            var td = ['<td class="min-width">' + icon + '</td>', name_html];

            if ($.isArray(weights) && weights.length) {
                for (var i = 0; i < weights.length; i += 1) {
                    var rate = item.rate[i] || '';
                    td.push('<td data-index="' + i + '"><input type="text" class="short numerical <?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-rate-input" value="' + rate + '" placeholder="0">&nbsp;<span class="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-currency">'+currency+'</span></td>');
                }
                td.push('<td></td>');
            } else {
                if($.isArray(item.rate)){
                    item.rate=toRate(item.rate.shift());
                }
                td.push('<td data-index="all"><input type="text" class="short numerical <?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-rate-input" value="' + (item.rate||'') + '" placeholder="0">&nbsp;<span class="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-currency">'+currency+'</span></td>');
            }
            td.push('<td>' + renderTranslitTimeSelector(item.transit_time) + '</td>');

            if (!region) {
                td.push('<td><a href="javascript:void(0);" class="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delete-country-link"><i class="icon16 delete"></i></a></td>');
            } else {
                td.push('<td><a href="javascript:void(0);" class="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-disable-region-toggle"><i class="icon10 ' + (!item.disabled ? 'no' : 'no-bw') + '"></i></a></td>');            }

            var cls = '';
            var title='';
            if (region && item.disabled) {
                cls = '<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-disabled';
                title = '<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['p']->value->_w("Shipping to this region will not be offered."), "js");?>
';
            }

            res += '<tr data-country="' + country + '" data-region="' + region + '" class="' + cls + '" title="'+title+'">' + td.join('') + '</tr>';

            if ($.isArray(item.items) && item.items.length) {
                res += renderDeliveryTableBodyRows(item.items, weights, currency);
            }

        }
        return res;
    };

    var renderTranslitTimeSelector = function(val) {
        val = typeof val === 'undefined' ? '+1 week' : val;
        var html = '<select class="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-transit-selector">';
        var selected = false;
        for (var i = 0; i < transits.length; i++) {
            selected = (!val && transits[i].default ) || (val === transits[i].value);
            html += '<option value="' + transits[i].value + '" ' + (selected ? 'selected="selected"' : '') + '>' + transits[i].title + '</option>';
        }
        html +='</select>';
        return html;
    };

    var renderCalculationTypeSelector = function() {
        return '<select class="<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-calculation-type" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[calculation_type]">' +
                        '<option value="all"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Same rate for any package weight');?>
</option>' +
                        '<option value="individually"><?php echo $_smarty_tpl->tpl_vars['p']->value->_w('Define rates by package weight...');?>
</option>' +
                '</select>';
    };

    var renderTable = function(items, weights) {
        formatValues();
        upHiddenInputs();
        $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table-head').html(renderDeliveryTableHead(weights));
        $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table-items').html(renderDeliveryTableBodyRows(items, weights));
    };

    renderTable(items, weights);


    var block = $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-deliverty-settings');

        $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-weight-unit-input').change(function(){
            var unit = $(this).val();
            $('.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-weight-unit').text(unit);
        }).change();

        $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-currency-input').change(function(){
            var currency = $(this).val();
            $('.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-currency').text(currency);
        }).change();

    //$('tr.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-disabled :input').attr('disabled',true);

    $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-add-new-country').change(function() {
        var select = $(this);
        var val = select.val();
        if (val) {

            select.find('option[value="' + val  + '"]').attr('disabled', true);
            select.find('option[value="%AL"]').attr('disabled', true);

            block.find('.empty-row').hide();
            var item = {
                country: val,
                rate: '',
                transit_time: '+1 week'
            };
            if ($.isArray(weights)) {
                item.rate = [];
                for (var i = 0; i < weights.length; i += 1) {
                    item.rate[i] = '';
                }
            }

            var mixinRegionItems = function(item, regions_data) {
                var region_items = [];
                var r_ord = regions_data.oOrder || [];
                var r_code;
                var r_codes = {
                };
                for (var r = 0; r < r_ord.length; r++) {
                    r_code = r_ord[r];
                    if ((r_code != '') && !r_codes[r_code]) {
                        r_codes[r_code] = r_code;
                        region_items.push({
                            country: val,
                            region: r_code,
                            rate: '',
                            transit_time: '+1 week'
                        });
                    }
                }
                item.items = region_items;
                return item;
            };

            var loadRegions = function(after) {
                if (regions[val] === undefined) {
                    var loading = $('<i class="icon16 loading"></i>').insertAfter(select);
                    $.post('<?php echo $_smarty_tpl->tpl_vars['xhr_url']->value;?>
', { country: val }, function(r) {
                        loading.remove();
                        regions[val] = r.data;
                        after(regions[val]);
                    }, 'json');
                } else {
                    after(regions[val]);
                }
            };

            loadRegions(function(regions_data) {
                item = mixinRegionItems(item, regions_data);
                items.push(item);
                $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table-items').append(renderDeliveryTableBodyRows([item], weights));
                upHiddenInputs();
            });

        }
        select.val('');
    });

    block.on('click', '.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-calculation-type', function() {
        var el = $(this);
        if (el.val() === 'individually') {
            for (var i = 0; i < items.length; i += 1) {
                var item = items[i];
                item.rate = [item.rate['all'] || ''];
            }
            weights = [''];
            renderTable(items, weights);
        }
    });

    block.on('click', '.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-add-column-link', function() {
        weights.push('');
        var last = weights.length - 1;
        for (var i = 0; i < items.length; i += 1) {
            var item = items[i];
            item.rate[last] = '';
        }
        renderTable(items, weights);
        upHiddenInputs();
    });

    block.on('click',  '.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delete-weight-link', function() {
        var link = $(this);
        var td = link.closest('.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-weight-td');
        var index = td.data('index');
        var ar_delete = function(ar, i) {
            return ar.slice(0, i).concat(ar.slice(i + 1));
        };
        for (var i = 0; i < items.length; i += 1) {
            var item = items[i];
            item.rate = ar_delete(item.rate, index);
        }

        weights = ar_delete(weights, index);
        renderTable(items, weights);
    });

    $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table-items')
        .on('change', 'input.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-rate-input', function() {
            var input = $(this);
            var td = input.closest('td');
            var index = td.data('index');
            if (index !== 'all') {
                index = parseInt(index, 10);
                if (isNaN(index)) {
                    return;
                }
            }
            var tr = input.closest('tr');
            var country = tr.data('country');
            var region = tr.data('region');
            var item = findByCountryAndRegion(items, country, region);
            if (!item) {
                return;
            }
            var val = toRate(input.val() || '');
            if (index === 'all') {
                item.rate = val;
            } else {
                if(!$.isArray(item.rate)){
                    item.rate=[];
                }
                item.rate[index] = val;
            }
            input.val(val);

            upHiddenInputs();

        })
        .on('change', 'select.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-transit-selector', function() {
            var selector = $(this);
            var tr = selector.closest('tr');
            var country = tr.data('country');
            var region = tr.data('region');
            var item = findByCountryAndRegion(items, country, region);
            if (!item) {
                return;
            }
            item.transit_time = selector.val();

            upHiddenInputs();

        })
        .on('click', '.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delete-country-link', function() {
            var tr = $(this).closest('tr');
            var country = tr.data('country');
            $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table-items tr[data-country="' + country + '"]').remove();
            items = deleteByCountry(items, country);
            $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-add-new-country').find('option[value="' + country + '"]').attr('disabled', false);
            if ($.isEmptyObject(items)) {
                $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-add-new-country').find('option[value="%AL"]').attr('disabled', false);
            }
            upHiddenInputs();

        })
        .on('click', '.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-disable-region-toggle', function() {
            var link = $(this);
            var icon = link.find('i');
            var tr = link.closest('tr');
            var country = tr.data('country');
            var region = tr.data('region');

            var item = findByCountryAndRegion(items, country, region);

            var delete_link = icon.hasClass('no');
            if (delete_link) {
                icon.removeClass('no').addClass('no-bw');
                tr.addClass('<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-disabled');
                tr.attr('title','<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['p']->value->_w("Shipping to this region will not be offered."), "js");?>
')
                //tr.find(':input').attr('disabled',true);
                item.disabled = true;
            } else {
                icon.removeClass('no-bw').addClass('no');
                tr.removeClass('<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-disabled');
                //tr.find(':input').attr('disabled',null);
                tr.attr('title','');
                delete item.disabled;
            }

            upHiddenInputs();
        })
        .on('click');

    $('#<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-delivery-table-head')
        .on('change', 'input.<?php echo $_smarty_tpl->tpl_vars['plid']->value;?>
-weight-input', function() {
            var input = $(this);
            var th = input.closest('th');
            var index = parseInt(th.data('index'), 10);
            if (isNaN(index) && index < weights.length) {
                return;
            }
            weights[index] = input.val();
            upHiddenInputs();
        });

})();</script>

<?php }} ?>