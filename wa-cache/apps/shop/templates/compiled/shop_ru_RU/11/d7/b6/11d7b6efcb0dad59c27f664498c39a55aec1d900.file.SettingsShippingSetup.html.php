<?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:16
         compiled from "/var/www/novikova_ss8olga/wa-apps/shop/templates/actions/settings/SettingsShippingSetup.html" */ ?>
<?php /*%%SmartyHeaderCode:13147357895eb29fc86049f2-15566549%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '11d7b6efcb0dad59c27f664498c39a55aec1d900' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-apps/shop/templates/actions/settings/SettingsShippingSetup.html',
      1 => 1584609754,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13147357895eb29fc86049f2-15566549',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'error' => 0,
    'plugin' => 0,
    'plugin_id' => 0,
    'taxes' => 0,
    'tax' => 0,
    'settings_html' => 0,
    'guide_html' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5eb29fc862eb38_89563203',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5eb29fc862eb38_89563203')) {function content_5eb29fc862eb38_89563203($_smarty_tpl) {?><?php if (!empty($_smarty_tpl->tpl_vars['error']->value)){?>
<h1 class="js-bread-crumbs"><a href="#/shipping/" class="back">&larr; Доставка</a>&nbsp;Ошибка</h1>
<span class="errormsg"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['error']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
<?php }else{ ?>
<form>
    <!-- GENERAL SHIPPING OPTION PARAMS -->
    <div class="field-group">

        <div class="block half-padded float-right">
            <ul class="menu-h with-icons">
            <?php if (isset($_smarty_tpl->tpl_vars['plugin']->value['status'])&&!empty($_smarty_tpl->tpl_vars['plugin']->value['id'])){?>
                <li><a href="#/shipping/plugin/delete/<?php echo $_smarty_tpl->tpl_vars['plugin']->value['id'];?>
/" class="js-action js-confirm" data-confirm-text="Этот способ доставки будет полностью удален. Продолжить?" style="position: relative; z-index: 99;"><i class="icon16 delete"></i>Удалить этот способ доставки</a></li>
            <?php }?>
            </ul>
        </div>

        <h1 class="js-bread-crumbs"><a href="#/shipping/" class="back">&larr; Доставка</a>&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['plugin']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</h1>

        <div class="field">
            <div class="name">
                <label for="s-shipping-plugin-status">Включен</label>
            </div>
            <div class="value">
                <input name="shipping[id]" type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['plugin_id']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                <input name="shipping[plugin]" type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['plugin']->value['plugin'], ENT_QUOTES, 'UTF-8', true);?>
">
                <input name="shipping[status]" type="checkbox"<?php if (!empty($_smarty_tpl->tpl_vars['plugin']->value['status'])){?> checked="checked"<?php }?> id="s-shipping-plugin-status" value="1">
            </div>
        </div>
        <div class="field">
            <div class="name">
                <label for="s-shipping-plugin-name">Название способа доставки</label>
            </div>
            <div class="value">
                <input name="shipping[name]" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['plugin']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" class="bold" id="s-shipping-plugin-name">
            </div>
        </div>
        <div class="field">
            <div class="name">
                <label for="s-shipping-plugin-logo">URL логотипа</label>
            </div>
            <div class="value">
                <input name="shipping[logo]" type="text" class="long" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['plugin']->value['logo'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" id="s-shipping-plugin-logo">
                <img src="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['plugin']->value['logo'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" class="s-payment-shipping-logo float-right">
                <p class="hint"><?php echo sprintf('Бесплатная коллекция иконок способов доставки доступна по адресу <a href="%1$s" target="_blank">%1$s</a> <i class="icon10 new-window"></i>','http://www.shop-script.ru/help/728/payment-shipping-icons/');?>
</p>
            </div>
        </div>
        <?php if (!empty($_smarty_tpl->tpl_vars['taxes']->value)){?>
        <div class="field">
            <div class="name"><label for="s-shipping-plugin-taxable">Облагается налогом</label></div>
            <div class="value">
                <select name="shipping[options][tax_id]" id="s-shipping-plugin-taxable">
                    <option value=""<?php if (empty($_smarty_tpl->tpl_vars['plugin']->value['options']['tax_id'])){?> selected<?php }?>>Нет</option>
                    <?php  $_smarty_tpl->tpl_vars['tax'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tax']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['taxes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tax']->key => $_smarty_tpl->tpl_vars['tax']->value){
$_smarty_tpl->tpl_vars['tax']->_loop = true;
?>
                        <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax']->value['id'], ENT_QUOTES, 'UTF-8', true);?>
"<?php if (!empty($_smarty_tpl->tpl_vars['plugin']->value['options']['tax_id'])&&($_smarty_tpl->tpl_vars['tax']->value['id']==$_smarty_tpl->tpl_vars['plugin']->value['options']['tax_id'])){?> selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                    <?php } ?>
                </select>
                <p class="hint">Выберите налоговое правило, которое должно применяться к стоимости доставки, рассчитанной этим плагином.</p>
            </div>
        </div>
        <?php }?>
        <div class="field">
            <div class="name"><label for="s-shipping-plugin-">Дополнительное время на комплектацию</label></div>
            <div class="value">
                <input type="number" name="shipping[options][assembly_time]" value="<?php if (isset($_smarty_tpl->tpl_vars['plugin']->value['options']['assembly_time'])){?><?php echo max(0,intval($_smarty_tpl->tpl_vars['plugin']->value['options']['assembly_time']));?>
<?php }?>" id="">
                <p class="hint">Укажите период времени в часах. Он будет добавлен ко времени готовности заказа с учетом режима работы витрины.</p>
            </div>
        </div>
        <div class="field">
            <div class="name">
                <label for="s-shipping-plugin-description">Описание</label>
            </div>
            <div class="value">
                <textarea name="shipping[description]" id="s-shipping-plugin-description"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['plugin']->value['description'], ENT_QUOTES, 'UTF-8', true);?>
</textarea>
                <p class="hint">Описание способа доставки, отображаемое на витрине при оформлении заказа (HTML).</p>
            </div>
        </div>
    </div>

    <div class="field-group">
        <div class="field">
            <div class="name">Предлагать доставку типам покупателей</div>
            <div class="value">
                <label><input type="radio" name="shipping[options][customer_type]" value="" <?php if (empty($_smarty_tpl->tpl_vars['plugin']->value['options']['customer_type'])){?> checked<?php }?>> Все</label>
            </div>
            <div class="value">
                <label><input type="radio" name="shipping[options][customer_type]" value="person" <?php if ((!empty($_smarty_tpl->tpl_vars['plugin']->value['options']['customer_type'])&&($_smarty_tpl->tpl_vars['plugin']->value['options']['customer_type']=='person'))){?> checked<?php }?>> Персоны</label>
            </div>
            <div class="value">
                <label><input type="radio" name="shipping[options][customer_type]" value="company" <?php if ((!empty($_smarty_tpl->tpl_vars['plugin']->value['options']['customer_type'])&&($_smarty_tpl->tpl_vars['plugin']->value['options']['customer_type']=='company'))){?> checked<?php }?>> Компании</label>
            </div>
            <div class="value hint"><?php echo sprintf('В режиме «%s» доставка предлагается только выбранным типам покупателей.<br>В режиме «%s» эта настройка игнорируется, и доставка всегда предлагается всем типам покупателей.','Оформление заказа в корзине','Пошаговое оформление заказа');?>
</div>
        </div>
    </div>

    <!-- CUSTOM SHIPPING MODULE PARAMS -->
    <div class="field-group">
        <?php echo $_smarty_tpl->tpl_vars['settings_html']->value;?>

    </div>


    <?php if (!empty('guide_html')){?>
    <div class="field-group">
        <?php echo $_smarty_tpl->tpl_vars['guide_html']->value;?>

    </div>
    <?php }?>

    <div class="field-group js-footer-block">
        <div class="field">
            <div class="value submit">
                <input type="submit" class="button green" value="Сохранить"> или <a href="#/shipping/" class="inline-link">отмена</a>
                <span id="settings-shipping-form-status" class="js-form-status" style="display:none"><!-- message placeholder --></span>
            </div>
        </div>
    </div>
</form>
<?php }?>
<?php }} ?>