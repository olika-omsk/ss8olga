<?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:54
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/settings.html" */ ?>
<?php /*%%SmartyHeaderCode:16487702955eb29fee827fd3-61462082%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '75d3909f92149ad8a18aa04263600b19339d8d6e' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/settings.html',
      1 => 1542209647,
      2 => 'file',
    ),
    'ae6d0c878c966039b38eee8cfd0d7bb865dcaa89' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/include.settings_basic_settings.html',
      1 => 1568104307,
      2 => 'file',
    ),
    '5afe1a44fab1918b8e3db2ad98e394b657e92854' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/include.settings_available_shipping.html',
      1 => 1566994296,
      2 => 'file',
    ),
    '480287aafe088d3e5821930e2fca8ca890d6c29f' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/include.settings_shipping_cost.html',
      1 => 1542122624,
      2 => 'file',
    ),
    'ec7e560352d5999b74227132686e6c0fb839313f' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/include.settings_work_time.html',
      1 => 1559819416,
      2 => 'file',
    ),
    'db4ccdf206d189ebf880eae63090416c15c8f8e2' => 
    array (
      0 => '/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/include.settings_additional.html',
      1 => 1545818508,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16487702955eb29fee827fd3-61462082',
  'function' => 
  array (
    'addDate' => 
    array (
      'parameter' => 
      array (
        'date' => 
        array (
        ),
        'type' => '',
        'iterator' => 0,
      ),
      'compiled' => '',
    ),
    'addDateTime' => 
    array (
      'parameter' => 
      array (
        'date' => 
        array (
        ),
        'type' => '',
        'iterator' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'wa_url' => 0,
    'wa' => 0,
    'regions_url' => 0,
    'settings' => 0,
    'weight_units' => 0,
    'length_units' => 0,
    'namespace' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5eb29fee94fce5_42682815',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5eb29fee94fce5_42682815')) {function content_5eb29fee94fce5_42682815($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['wa_url']->value;?>
wa-plugins/shipping/sd/js/settings.js?<?php echo $_smarty_tpl->tpl_vars['wa']->value->version(true);?>
"></script>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['wa_url']->value;?>
wa-plugins/shipping/sd/css/sd.css?<?php echo $_smarty_tpl->tpl_vars['wa']->value->version(true);?>
">

<div id="js-shipping-sd-settings">
    
    <?php /*  Call merged included template "./include.settings_basic_settings.html" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('./include.settings_basic_settings.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '16487702955eb29fee827fd3-61462082');
content_5eb29fee832d48_77368280($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "./include.settings_basic_settings.html" */?>

    
    <?php /*  Call merged included template "./include.settings_available_shipping.html" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('./include.settings_available_shipping.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '16487702955eb29fee827fd3-61462082');
content_5eb29fee86bac3_11716317($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "./include.settings_available_shipping.html" */?>

    
    <?php /*  Call merged included template "./include.settings_shipping_cost.html" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('./include.settings_shipping_cost.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '16487702955eb29fee827fd3-61462082');
content_5eb29fee8a0691_23313970($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "./include.settings_shipping_cost.html" */?>

    
    <?php /*  Call merged included template "./include.settings_work_time.html" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('./include.settings_work_time.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '16487702955eb29fee827fd3-61462082');
content_5eb29fee8c5db0_13310113($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "./include.settings_work_time.html" */?>

    
    <?php /*  Call merged included template "./include.settings_additional.html" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('./include.settings_additional.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '16487702955eb29fee827fd3-61462082');
content_5eb29fee929ec3_12612648($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "./include.settings_additional.html" */?>
</div>

<script>
    (function ($) {
        new systemShippingSDPluginSettings({
            $wrapper: $("#js-shipping-sd-settings"),
            regions_url: <?php echo json_encode($_smarty_tpl->tpl_vars['regions_url']->value);?>
,
            current_currency: <?php echo json_encode((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['currency'])===null||$tmp==='' ? '' : $tmp));?>
,
            current_weight: <?php echo json_encode(ifset($_smarty_tpl->tpl_vars['weight_units']->value,$_smarty_tpl->tpl_vars['settings']->value['weight_unit'],''));?>
,
            current_length: <?php echo json_encode(ifset($_smarty_tpl->tpl_vars['length_units']->value,$_smarty_tpl->tpl_vars['settings']->value['length_unit'],''));?>
,
            namespace: <?php echo json_encode($_smarty_tpl->tpl_vars['namespace']->value);?>
,
            date_format: <?php echo json_encode(waDateTime::getFormatJs('date'));?>
,
            templates: {
                new_datetime_html: <?php ob_start();?><?php smarty_template_function_addDateTime($_smarty_tpl,array());?>
<?php echo json_encode(ob_get_clean())?>,
                new_date_html: <?php ob_start();?><?php smarty_template_function_addDate($_smarty_tpl,array());?>
<?php echo json_encode(ob_get_clean())?>,
                error_date_html: <?php echo json_encode(Smarty::$_smarty_vars['capture']['_error_date_html']);?>
,
                new_photo_html: <?php echo json_encode(Smarty::$_smarty_vars['capture']['_new_photo_html']);?>

            }
        });
    })(jQuery);
</script><?php }} ?><?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:54
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/include.settings_basic_settings.html" */ ?>
<?php if ($_valid && !is_callable('content_5eb29fee832d48_77368280')) {function content_5eb29fee832d48_77368280($_smarty_tpl) {?><div class="field-group">
    <div class="field">
        <div class="name"></div>
        <div class="value"><h3><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Basic settings');?>
</h3></div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Currency');?>
</div>
        <div class="value">
            <select name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[currency]" class="js-sd-select-currency">
                <?php if (!empty($_smarty_tpl->tpl_vars['currencies']->value)){?>
                    <?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value){
$_smarty_tpl->tpl_vars['currency']->_loop = true;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['currency']->value['code'];?>
" <?php if ($_smarty_tpl->tpl_vars['currency']->value['code']==htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['currency'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true)){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['currency']->value['code'];?>
</option>
                    <?php } ?>
                <?php }?>
            </select>
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Supported payment types');?>
</div>
        <div class="value">
            <?php if (!empty($_smarty_tpl->tpl_vars['payment_type']->value)){?>
            <?php $_smarty_tpl->tpl_vars['payment_types'] = new Smarty_variable(ifset($_smarty_tpl->tpl_vars['settings']->value,'payment_type',array()), null, 0);?>
                <?php  $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['type']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['payment_type']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['type']->key => $_smarty_tpl->tpl_vars['type']->value){
$_smarty_tpl->tpl_vars['type']->_loop = true;
?>
                    <input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[payment_type][]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
"
                           <?php if (in_array(htmlspecialchars($_smarty_tpl->tpl_vars['type']->value['value'], ENT_QUOTES, 'UTF-8', true),$_smarty_tpl->tpl_vars['payment_types']->value)){?>checked<?php }?>> <?php echo $_smarty_tpl->tpl_vars['type']->value['title'];?>

                    <br>
                <?php } ?>
            <?php }?><br>
            <p class="hint">
                <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Select payment types which must be supported by a shipping option selected by a customer.');?>
 <br>
                <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('This setting is not used for multi-step checkout.');?>

            </p>
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Weight dimension unit');?>
</div>
        <div class="value">
            <select name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[weight_unit]" class="js-sd-select-weight">
                <?php  $_smarty_tpl->tpl_vars['wu_name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['wu_name']->_loop = false;
 $_smarty_tpl->tpl_vars['wu'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['weight_units']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['wu_name']->key => $_smarty_tpl->tpl_vars['wu_name']->value){
$_smarty_tpl->tpl_vars['wu_name']->_loop = true;
 $_smarty_tpl->tpl_vars['wu']->value = $_smarty_tpl->tpl_vars['wu_name']->key;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['wu']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['settings']->value['weight_unit']==$_smarty_tpl->tpl_vars['wu']->value){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['wu_name']->value;?>
</option>
                <?php } ?>
            </select>
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Length dimension unit');?>
</div>
        <div class="value">
            <select name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[length_unit]" class="js-sd-select-length">
                <?php  $_smarty_tpl->tpl_vars['lu_name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lu_name']->_loop = false;
 $_smarty_tpl->tpl_vars['lu'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['length_units']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lu_name']->key => $_smarty_tpl->tpl_vars['lu_name']->value){
$_smarty_tpl->tpl_vars['lu_name']->_loop = true;
 $_smarty_tpl->tpl_vars['lu']->value = $_smarty_tpl->tpl_vars['lu_name']->key;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['lu']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['settings']->value['length_unit']==$_smarty_tpl->tpl_vars['lu']->value){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['lu_name']->value;?>
</option>
                <?php } ?>
            </select>
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Service');?>
</div>
        <div class="value">
            <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[service]" value="<?php echo ifset($_smarty_tpl->tpl_vars['settings']->value,'service','');?>
">
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Address');?>
</div>
        <div class="value">
            <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[address]" value="<?php echo ifset($_smarty_tpl->tpl_vars['settings']->value,'address','');?>
">
        </div>
    </div>

    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Shipping time');?>

        </div>
        <div class="value">
            <input type="number"
                   name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[processing]"
                   value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['processing'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
"
                   placeholder="0"
                   min="0"> <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('hours');?>

            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Time required to deliver ordered items to this pickup point, which is added to the total order ready time.');?>
</p>
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Storage time');?>
</div>
        <div class="value">
            <input type="number" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[storage_days]" value="<?php echo ifset($_smarty_tpl->tpl_vars['settings']->value,'storage_days','');?>
" min="0"> <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('days');?>

        </div>
    </div>
</div><?php }} ?><?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:54
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/include.settings_available_shipping.html" */ ?>
<?php if ($_valid && !is_callable('content_5eb29fee86bac3_11716317')) {function content_5eb29fee86bac3_11716317($_smarty_tpl) {?><div class="field-group">
    <div class="field">
        <div class="name"></div>
        <div class="value"><h3><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Availability options');?>
</h3></div>
    </div>

    
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Country');?>
</div>
        <div class="value">
            <i class="icon16 loading js-sd-country-loader" style="display:none; margin-left: -23px;"></i>
            <select name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[country]" class="js-sd-select-country" required>
                <?php  $_smarty_tpl->tpl_vars['country'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['country']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['country']->key => $_smarty_tpl->tpl_vars['country']->value){
$_smarty_tpl->tpl_vars['country']->_loop = true;
?>
                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value['iso3letter'], ENT_QUOTES, 'UTF-8', true);?>
"
                            <?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['country'])&&htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['country'], ENT_QUOTES, 'UTF-8', true)==$_smarty_tpl->tpl_vars['country']->value['iso3letter']){?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value['name'], ENT_QUOTES, 'UTF-8', true);?>

                    </option>
                <?php } ?>
            </select>
            <br>
            <span class="errormsg js-sd-country-errormsg" style="display: none"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Please select a country.');?>
</span>
        </div>
    </div>

    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Region');?>

        </div>
        <div class="value">
            <div class="js-sd-regions-msg" <?php if (!empty($_smarty_tpl->tpl_vars['regions']->value)){?>style="display: none;"<?php }?>>
                <p class="small">
                    <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Shipping will be restricted to the selected country. Rates defined below will be applied to the entire country.');?>

                </p>
            </div>
            <div class="js-sd-regions" <?php if (empty($_smarty_tpl->tpl_vars['regions']->value)){?>style="display: none;"<?php }?>>
                <?php $_smarty_tpl->tpl_vars['current_region'] = new Smarty_variable(ifset($_smarty_tpl->tpl_vars['settings']->value,'region',''), null, 0);?>

                <select name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[region]" class="js-sd-select-region">
                    <option value=""></option>
                    <?php  $_smarty_tpl->tpl_vars['region'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['region']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['regions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['region']->key => $_smarty_tpl->tpl_vars['region']->value){
$_smarty_tpl->tpl_vars['region']->_loop = true;
?>
                        <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['region']->value['code'], ENT_QUOTES, 'UTF-8', true);?>
"<?php if (htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['region'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true)==$_smarty_tpl->tpl_vars['region']->value['code']){?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['region']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                    <?php } ?>
                </select>
                <p class="small"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Shipping will be restricted to the selected region.');?>
</p>
            </div>
        </div>
    </div>

    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Localities');?>

        </div>
        <div class="value">
            <input name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[city]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['city'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="text">

            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Separate localities with a comma');?>
</p>
        </div>
    </div>

    
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Coordinates');?>
</div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Latitude');?>
</div>
        <div class="value">
            <input name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[latitude]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['latitude'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Longitude');?>
</div>
        <div class="value">
            <input name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[longitude]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['longitude'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Use this service to get coordinates:');?>
 <a href="https://yandex.ru/map-constructor/location-tool/" target="_blank">Yandex Map
                    Constructor</a></p>
        </div>
    </div>

    
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Max weight');?>
</div>
        <div class="value">
            <input class="js-sd-weight" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[max_weight]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_weight'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Max length');?>
</div>
        <div class="value">
            <input class="js-sd-length" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[max_length]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_length'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Max width');?>
</div>
        <div class="value">
            <input class="js-sd-length" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[max_width]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_width'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Max height');?>
</div>
        <div class="value">
            <input class="js-sd-length" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[max_height]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['max_height'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Use data provided by special plugins for comparison with common order dimensions. If no order dimensions data are available, the maximum length, width, and height values of order items are used.');?>
</p>
        </div>
    </div>
</div><?php }} ?><?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:54
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/include.settings_shipping_cost.html" */ ?>
<?php if ($_valid && !is_callable('content_5eb29fee8a0691_23313970')) {function content_5eb29fee8a0691_23313970($_smarty_tpl) {?><div class="field-group">
    
    <div class="field">
        <div class="name"></div>
        <div class="value"><h3><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Shipping cost');?>
</h3></div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Minimum order cost for free delivery');?>
</div>
        <div class="value">
            <input class="js-sd-currency" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[free_shipping]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['free_shipping'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Base cost and markups are ignored when free delivery is applied.');?>
</p>
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Base cost');?>
</div>
        <div class="value">
            <input class="js-sd-currency" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[basic_shipping]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['basic_shipping'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any" required>
        </div>
    </div>

    
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Markup for extra order weight');?>
</div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Threshold weight');?>
</div>
        <div class="value">
            <input class="js-sd-weight" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[markup_weight]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['markup_weight'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('');?>
</p>
        </div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Markup for weight');?>
</div>
        <div class="value">
            <input class="js-sd-currency" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[markup_weight_price]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['markup_weight_price'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('');?>
</p>
        </div>
    </div>

    
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Markup for extra order dimensions');?>
</div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Threshold length');?>
</div>
        <div class="value">
            <input class="js-sd-length" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[markup_length]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['markup_length'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('');?>
</p>
        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Threshold width');?>
</div>
        <div class="value">
            <input class="js-sd-length" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[markup_width]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['markup_width'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('');?>
</p>
        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Threshold height');?>
</div>
        <div class="value">
            <input class="js-sd-length" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[markup_height]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['markup_height'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('');?>
</p>
        </div>
    </div>
    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Markup for order dimensions');?>
</div>
        <div class="value">
            <input class="js-sd-currency" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[markup_size_price]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['markup_size_price'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
" type="number" min="0" step="any">
            <p class="hint"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Use data provided by special plugins for comparison with common order dimensions. If no order dimensions data are available, the maximum length, width, and height values of order items are used.');?>
</p>
        </div>
    </div>
</div><?php }} ?><?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:54
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/include.settings_work_time.html" */ ?>
<?php if ($_valid && !is_callable('content_5eb29fee8c5db0_13310113')) {function content_5eb29fee8c5db0_13310113($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_wa_date')) include '/var/www/novikova_ss8olga/wa-system/vendors/smarty-plugins/modifier.wa_date.php';
?><?php if (!function_exists('smarty_template_function_addDate')) {
    function smarty_template_function_addDate($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['addDate']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
    <tr>
        <td>
            <input type="text"
                   class="js-sd-date js-datepicker"
                   value="<?php echo htmlspecialchars(smarty_modifier_wa_date(ifset($_smarty_tpl->tpl_vars['date']->value,'date','')), ENT_QUOTES, 'UTF-8', true);?>
"
                   name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['iterator']->value;?>
][date]"
                   data-name="date"
                   placeholder="<?php echo waDateTime::format('date');?>
"
                   autocomplete="off"
                   required>
        </td>
        <td>
            <input type="text"
                   maxlength="64"
                   class="js-sd-time-additional"
                   name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['iterator']->value;?>
][additional]"
                   value="<?php echo htmlspecialchars(ifset($_smarty_tpl->tpl_vars['date']->value,'additional',''), ENT_QUOTES, 'UTF-8', true);?>
"
                   data-name="additional"
                   >
        </td>
        <td>
            <a class="js-sd-delete-date" href="#" data-type="<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
">
                <i class="icon16 no"></i>
            </a>
        </td>
    </tr>
<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>


<?php if (!is_callable('smarty_modifier_wa_date')) include '/var/www/novikova_ss8olga/wa-system/vendors/smarty-plugins/modifier.wa_date.php';
?><?php if (!function_exists('smarty_template_function_addDateTime')) {
    function smarty_template_function_addDateTime($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['addDateTime']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
    <tr>
        <td>
            <input type="text"
                   class="js-sd-date js-datepicker"
                   value="<?php echo htmlspecialchars(smarty_modifier_wa_date(ifset($_smarty_tpl->tpl_vars['date']->value,'date','')), ENT_QUOTES, 'UTF-8', true);?>
"
                   name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['iterator']->value;?>
][date]"
                   data-name="date"
                   placeholder="<?php echo waDateTime::format('date');?>
"
                   autocomplete="off"
                   required>
        </td>
        <td>
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('from');?>
 <input type="time"
                                      class="js-sd-time-start"
                                      name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['iterator']->value;?>
][start_work]"
                                      value="<?php echo htmlspecialchars(ifset($_smarty_tpl->tpl_vars['date']->value,'start_work','10:00'), ENT_QUOTES, 'UTF-8', true);?>
"
                                      data-name="start_work"
                                      required>
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('to');?>
 <input type="time"
                                    class="js-sd-time-end"
                                    name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['iterator']->value;?>
][end_work]"
                                    value="<?php echo htmlspecialchars(ifset($_smarty_tpl->tpl_vars['date']->value,'end_work','19:00'), ENT_QUOTES, 'UTF-8', true);?>
"
                                    data-name="end_work"
                                    required>
        </td>
        <td>
            <input type="time"
                   class="js-sd-end-process"
                   name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['iterator']->value;?>
][end_process]"
                   value="<?php echo htmlspecialchars(ifset($_smarty_tpl->tpl_vars['date']->value,'end_process','14:00'), ENT_QUOTES, 'UTF-8', true);?>
"
                   data-name="end_process"
                   required>
        </td>
        <td>
            <input type="text"
                   maxlength="64"
                   class="js-sd-time-additional"
                   name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['iterator']->value;?>
][additional]"
                   value="<?php echo htmlspecialchars(ifset($_smarty_tpl->tpl_vars['date']->value,'additional',''), ENT_QUOTES, 'UTF-8', true);?>
"
                   data-name="additional"
                   >
        </td>

        <td>
            <a class="js-sd-delete-date" href="#" data-type="<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
">
                <i class="icon16 no"></i>
            </a>
        </td>
    </tr>
<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>



<div class="field-group">
    <div class="field">
        <div class="name"></div>
        <div class="value"><h3><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Working time');?>
</h3></div>
    </div>

    <div class="field">
        <div class="name"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Time zone');?>
</div>
        <div class="value">
            <select name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[timezone]">
                <?php  $_smarty_tpl->tpl_vars['cities'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cities']->_loop = false;
 $_smarty_tpl->tpl_vars['timezone'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['timezones']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cities']->key => $_smarty_tpl->tpl_vars['cities']->value){
$_smarty_tpl->tpl_vars['cities']->_loop = true;
 $_smarty_tpl->tpl_vars['timezone']->value = $_smarty_tpl->tpl_vars['cities']->key;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['timezone']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['timezone']->value==htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['timezone'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true)){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['cities']->value;?>
</option>
                <?php } ?>
            </select>
        </div>
    </div>

    <div class="field">
        <div class="value js-sd-worktime-wrapper">
            <table class="zebra">
                <thead>
                <tr>
                    <th><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Day of the week');?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Works');?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Working time');?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Orders for pickup');?>
</th>
                    <th><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Description');?>
</th>
                </tr>
                </thead>
                <tbody>

                <?php  $_smarty_tpl->tpl_vars['day_data'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['day_data']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['weekdays']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['day_data']->key => $_smarty_tpl->tpl_vars['day_data']->value){
$_smarty_tpl->tpl_vars['day_data']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['day_data']->key;
?>
                    <?php $_smarty_tpl->tpl_vars['is_works'] = new Smarty_variable(ifset($_smarty_tpl->tpl_vars['day_data']->value,'works',false), null, 0);?>
                    <tr class="day">
                        <td><?php echo $_smarty_tpl->tpl_vars['day_data']->value['name'];?>
</td>
                        <td>
                            <input type="checkbox"
                                   value="1"
                                   name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[weekdays][<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
][works]"
                                   class="js-sd-worktime-toggle"
                                   <?php if ($_smarty_tpl->tpl_vars['is_works']->value){?>checked<?php }?>>
                        </td>
                        <td class="js-sd-time-wrapper">
                            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('from');?>
 <input type="time"
                                                      name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[weekdays][<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
][start_work]"
                                                      value="<?php echo ifset($_smarty_tpl->tpl_vars['day_data']->value,'start_work','');?>
"
                                                      placeholder="10:00"
                                                      class="js-sd-time-start"
                                                      <?php if ($_smarty_tpl->tpl_vars['is_works']->value){?>required<?php }?>>
                            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('to');?>
 <input type="time"
                                                    name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[weekdays][<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
][end_work]"
                                                    value="<?php echo ifset($_smarty_tpl->tpl_vars['day_data']->value,'end_work','');?>
"
                                                    placeholder="19:00"
                                                    class="js-sd-time-end"
                                                    <?php if ($_smarty_tpl->tpl_vars['is_works']->value){?>required<?php }?>>
                        </td>
                        <td>
                            <input type="time"
                                   class="js-sd-end-process"
                                   name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[weekdays][<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
][end_process]"
                                   value="<?php echo ifset($_smarty_tpl->tpl_vars['day_data']->value,'end_process','');?>
"
                                   <?php if ($_smarty_tpl->tpl_vars['is_works']->value){?>required<?php }?>
                                   >
                        </td>
                        <td>
                            <input type="text"
                                   maxlength="64"
                                   class="js-sd-time-additional"
                                   name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[weekdays][<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
][additional]"
                                   value="<?php echo ifset($_smarty_tpl->tpl_vars['day_data']->value,'additional','');?>
">
                        </td>
                    </tr>
                <?php } ?>

                </tbody>
            </table>
            <p class="hint"><?php echo sprintf($_smarty_tpl->tpl_vars['obj']->value->_w('Values in “%s” column denote the last hour of a pickup point’s workday until which orders are accepted to be ready for pickup by customers on the same day. Orders received after that hour are ready for pickup on the next workday.'),$_smarty_tpl->tpl_vars['obj']->value->_w('Orders for pickup'));?>
</p>
        </div>
    </div>
    <br>
    
    <div class="field-group">
        <div class="field">
            <div class="name">
                <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Extra workdays');?>

            </div>
            <div class="value js-sd-worktime-wrapper">
                <table class="zebra js-sd-workdays">
                    <thead>
                    <tr>
                        <th><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Date');?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Time');?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Orders for pickup');?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Description');?>
</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr class="white">
                        <td><a class="inline-link js-sd-add-date" data-type="workdays"><i class="icon16 add"></i><b><i><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Add date');?>
</i></b></a></td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['workdays'])){?>
                        <?php  $_smarty_tpl->tpl_vars['workdate'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['workdate']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['settings']->value['workdays']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['workdate']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['workdate']->key => $_smarty_tpl->tpl_vars['workdate']->value){
$_smarty_tpl->tpl_vars['workdate']->_loop = true;
 $_smarty_tpl->tpl_vars['workdate']->iteration++;
?>
                            <?php smarty_template_function_addDateTime($_smarty_tpl,array('date'=>$_smarty_tpl->tpl_vars['workdate']->value,'type'=>'workdays','iterator'=>$_smarty_tpl->tpl_vars['workdate']->iteration));?>

                        <?php } ?>
                    <?php }?>
                    </tbody>
                </table>
                <br><br>
            </div>
        </div>
    </div>
    <br>
    
    <div class="field-group">
        <div class="field">
            <div class="name">
                <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Extra days off');?>

            </div>
            <div class="value js-sd-worktime-wrapper">
                <table class="zebra js-sd-weekend">
                    <thead>
                    <tr>
                        <th class="align-center"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Date');?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Description');?>
</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr class="white">
                        <td><a class="inline-link js-sd-add-date" data-type="weekend"><i class="icon16 add"></i><b><i><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Add date');?>
</i></b></a></td>
                        <td>&nbsp;</td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['weekend'])){?>
                        <?php  $_smarty_tpl->tpl_vars['weekend_date'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['weekend_date']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['settings']->value['weekend']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['weekend_date']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['weekend_date']->key => $_smarty_tpl->tpl_vars['weekend_date']->value){
$_smarty_tpl->tpl_vars['weekend_date']->_loop = true;
 $_smarty_tpl->tpl_vars['weekend_date']->iteration++;
?>
                            <?php smarty_template_function_addDate($_smarty_tpl,array('date'=>$_smarty_tpl->tpl_vars['weekend_date']->value,'type'=>'weekend','iterator'=>$_smarty_tpl->tpl_vars['weekend_date']->iteration));?>

                        <?php } ?>
                    <?php }?>
                    </tbody>
                </table>
                <br><br>
            </div>
        </div>
    </div>
</div>

<?php $_smarty_tpl->_capture_stack[0][] = array("_error_date_html", null, null); ob_start(); ?>
    <p class="js-sd-error-date-msg errormsg"><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Invalid date');?>
</p>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?><?php }} ?><?php /* Smarty version Smarty-3.1.14, created on 2020-05-06 16:30:54
         compiled from "/var/www/novikova_ss8olga/wa-plugins/shipping/sd/templates/include.settings_additional.html" */ ?>
<?php if ($_valid && !is_callable('content_5eb29fee929ec3_12612648')) {function content_5eb29fee929ec3_12612648($_smarty_tpl) {?><div class="field-group">
    <div class="field">
        <div class="name"></div>
        <div class="value"><h3><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Additional info');?>
</h3></div>
    </div>

    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('How to reach');?>

        </div>
        <div class="value">
            <textarea name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[way]"><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['way'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
</textarea>
        </div>
    </div>

    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Description');?>

        </div>
        <div class="value">
            <textarea name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[additional]"><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['settings']->value['additional'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8', true);?>
</textarea>
        </div>
    </div>

    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Upload a photo');?>

        </div>
        <div class="value">
            <input type="file" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[photo]" class="js-upload-image">
        </div>
    </div>

    <div class="field">
        <div class="name">
            <?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('Photos');?>

        </div>
        <div class="value">
            <ul class="sd-images js-sd-images">
                <?php if ((!empty($_smarty_tpl->tpl_vars['settings']->value['photos']))){?>
                    <?php  $_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['photo']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['settings']->value['photos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['photo']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->key => $_smarty_tpl->tpl_vars['photo']->value){
$_smarty_tpl->tpl_vars['photo']->_loop = true;
 $_smarty_tpl->tpl_vars['photo']->iteration++;
?>
                        <li>
                            <a href="javascript:void(0);" class="js-sd-image">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['photo']->value['uri'];?>
">
                                <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[photos][<?php echo $_smarty_tpl->tpl_vars['photo']->iteration;?>
][uri]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['photo']->value['uri'], ENT_QUOTES, 'UTF-8', true);?>
" class="js-sd-image-uri">
                            </a>
                            <a class="js-sd-image-delete" href="javascript:void(0);">
                                <i class="icon16 delete"></i><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('remove');?>

                            </a>
                        </li>
                    <?php } ?>
                <?php }?>
            </ul>
        </div>
    </div>
</div>

<?php $_smarty_tpl->_capture_stack[0][] = array("_new_photo_html", null, null); ob_start(); ?>
    <li>
        <a class="js-sd-image" href="javascript:void(0);">
            <img src="%s">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
[photos][]" value="%s">
        </a>
        <a class="js-sd-image-delete" href="javascript:void(0);">
            <i class="icon16 delete"></i><?php echo $_smarty_tpl->tpl_vars['obj']->value->_w('remove');?>

        </a>
    </li>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?><?php }} ?>