<?php
return array (
  0 => 
  array (
    'app_id' => 'shop',
    'name' => 'csv.upload_app',
    'value' => 'shop',
  ),
  1 => 
  array (
    'app_id' => 'shop',
    'name' => 'country',
    'value' => 'rus',
  ),
  2 => 
  array (
    'app_id' => 'shop',
    'name' => 'currency',
    'value' => 'RUB',
  ),
  3 => 
  array (
    'app_id' => 'shop',
    'name' => 'use_product_currency',
    'value' => 'true',
  ),
  4 => 
  array (
    'app_id' => 'shop',
    'name' => 'shipping_payment_disabled',
    'value' => '[]',
  ),
  5 => 
  array (
    'app_id' => 'shop',
    'name' => 'update_time',
    'value' => '1582730959',
  ),
  6 => 
  array (
    'app_id' => 'shop.migrate',
    'name' => 'update_time',
    'value' => '1',
  ),
  7 => 
  array (
    'app_id' => 'shop',
    'name' => 'tutorial_passed_steps',
    'value' => '["welcome"]',
  ),
  8 => 
  array (
    'app_id' => 'shop',
    'name' => 'csv.upload_path',
    'value' => 'path/to/folder/with/source/images/',
  ),
  9 => 
  array (
    'app_id' => 'shop',
    'name' => 'list_columns',
    'value' => 'sku_count',
  ),
  10 => 
  array (
    'app_id' => 'shop',
    'name' => 'preview_hash',
    'value' => '5ca71e2dd6e35.1554456109',
  ),
  11 => 
  array (
    'app_id' => 'shop',
    'name' => 'theme_hash',
    'value' => '5d9f2e7e82213.1570713214',
  ),
  12 => 
  array (
    'app_id' => 'shop',
    'name' => 'setup_demo_time',
    'value' => '1588764570',
  ),
);
