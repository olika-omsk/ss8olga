<?php
return array (
  'boxberryShippingGetSettings' => 'wa-plugins/shipping/boxberry/lib/classes/boxberryShippingGetSettings.class.php',
  'boxberryShippingDraftPackage' => 'wa-plugins/shipping/boxberry/lib/classes/boxberryShippingDraftPackage.class.php',
  'boxberryShippingApiManager' => 'wa-plugins/shipping/boxberry/lib/classes/boxberryShippingApiManager.class.php',
  'boxberryShippingHandbookPointDescription' => 'wa-plugins/shipping/boxberry/lib/classes/handbook/boxberryShippingHandbookPointDescription.class.php',
  'boxberryShippingHandbookManager' => 'wa-plugins/shipping/boxberry/lib/classes/handbook/boxberryShippingHandbookManager.class.php',
  'boxberryShippingHandbookPointsForParcels' => 'wa-plugins/shipping/boxberry/lib/classes/handbook/boxberryShippingHandbookPointsForParcels.class.php',
  'boxberryShippingHandbookCityZips' => 'wa-plugins/shipping/boxberry/lib/classes/handbook/boxberryShippingHandbookCityZips.class.php',
  'boxberryShippingHandbookCityRegions' => 'wa-plugins/shipping/boxberry/lib/classes/handbook/boxberryShippingHandbookCityRegions.class.php',
  'boxberryShippingHandbookAvailablePoints' => 'wa-plugins/shipping/boxberry/lib/classes/handbook/boxberryShippingHandbookAvailablePoints.class.php',
  'boxberryShippingViewHelper' => 'wa-plugins/shipping/boxberry/lib/classes/boxberryShippingViewHelper.class.php',
  'boxberryShippingCalculateValidate' => 'wa-plugins/shipping/boxberry/lib/classes/calculate/boxberryShippingCalculateValidate.class.php',
  'boxberryShippingCalculateCourier' => 'wa-plugins/shipping/boxberry/lib/classes/calculate/boxberryShippingCalculateCourier.class.php',
  'boxberryShippingCalculateHelper' => 'wa-plugins/shipping/boxberry/lib/classes/calculate/boxberryShippingCalculateHelper.class.php',
  'boxberryShippingCalculateInterface' => 'wa-plugins/shipping/boxberry/lib/classes/calculate/boxberryShippingCalculate.interface.php',
  'boxberryShippingCalculatePoints' => 'wa-plugins/shipping/boxberry/lib/classes/calculate/boxberryShippingCalculatePoints.class.php',
);
