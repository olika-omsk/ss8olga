<?php
return array (
  'ss8olga.dev.echo-company.ru' => 
  array (
    0 => 
    array (
      'url' => 'contacts/*',
      'app' => 'contacts',
      'locale' => 'ru_RU',
      'private' => true,
    ),
    1 => 
    array (
      'url' => 'shop/*',
      'app' => 'shop',
      'theme' => 'hypermarket',
      'theme_mobile' => 'hypermarket',
      'checkout_version' => '2',
      'locale' => 'ru_RU',
      'title' => '',
      'meta_keywords' => '',
      'meta_description' => '',
      'og_title' => '',
      'og_image' => '',
      'og_video' => '',
      'og_description' => '',
      'og_type' => '',
      'og_url' => '',
      'url_type' => '0',
      'type_id' => '0',
      'currency' => 'RUB',
      'public_stocks' => '0',
      'drop_out_of_stock' => '0',
      'payment_id' => '0',
      'shipping_id' => '0',
      'checkout_storefront_id' => '6278f413e1bd6fb5bc5ded1c9944a64d',
    ),
    2 => 
    array (
      'url' => '*',
      'app' => 'site',
      'locale' => 'ru_RU',
      'theme' => 'hypermarket',
      'theme_mobile' => 'hypermarket',
    ),
  ),
);
