<?php
return array (
  'apps' => 
  array (
    0 => 
    array (
      'url' => '/company/',
      'name' => 'О компании',
    ),
    1 => 
    array (
      'url' => '/dostavka/',
      'name' => 'Доставка',
    ),
    2 => 
    array (
      'url' => '/oplata/',
      'name' => 'Оплата',
    ),
    3 => 
    array (
      'url' => '/vozvrat/',
      'name' => 'Возврат',
    ),
    4 => 
    array (
      'url' => '/contacts/',
      'name' => 'Контакты',
    ),
  ),
);
